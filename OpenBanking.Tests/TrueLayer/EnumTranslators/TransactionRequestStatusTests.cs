﻿using OpenBanking.Models;
using OpenBanking.TrueLayer.EnumTranslators;
using OpenBanking.TrueLayer.Models;

namespace OpenBanking.Tests.TrueLayer.EnumTranslators;

public static class TransactionRequestStatusTests
{
    [Theory]
    [InlineData(TransactionsPollResponse.RequestStatus.Succeeded, OpenBankingTransactionsResponse.RequestStatusType.Succeeded)]
    [InlineData(TransactionsPollResponse.RequestStatus.Failed, OpenBankingTransactionsResponse.RequestStatusType.Failed)]
    [InlineData(TransactionsPollResponse.RequestStatus.Queued, OpenBankingTransactionsResponse.RequestStatusType.Queued)]
    [InlineData(TransactionsPollResponse.RequestStatus.Running, OpenBankingTransactionsResponse.RequestStatusType.Running)]
    public static void CanConvert(
        TransactionsPollResponse.RequestStatus from,
        OpenBankingTransactionsResponse.RequestStatusType to)
    {
        Assert.Equal(from.Translate(), to);
    }
}