﻿using OpenBanking.Models;
using OpenBanking.TrueLayer.EnumTranslators;

namespace OpenBanking.Tests.TrueLayer.EnumTranslators;

public static class TransactionTypeEnumTests
{
    [Theory]
    [InlineData("credit", OpenBankingTransaction.TransactionTypeEnum.Credit)]
    [InlineData("CREDIT", OpenBankingTransaction.TransactionTypeEnum.Credit)]
    [InlineData("debit", OpenBankingTransaction.TransactionTypeEnum.Debit)]
    [InlineData("DEBIT", OpenBankingTransaction.TransactionTypeEnum.Debit)]
    public static void CanTranslate(string from, OpenBankingTransaction.TransactionTypeEnum e)
    {
        Assert.Equal(e, TransactionTypeEnumTranslator.Translate(from));
    }

    [Fact]
    public static void UnknownStringThrows()
    {
        Assert.Throws<InvalidDataException>(() => TransactionTypeEnumTranslator.Translate("unknown"));
    }
}