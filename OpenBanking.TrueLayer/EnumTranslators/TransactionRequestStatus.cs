﻿using OpenBanking.Models;
using OpenBanking.TrueLayer.Models;

namespace OpenBanking.TrueLayer.EnumTranslators;

internal static class TransactionRequestStatus
{
    public static OpenBankingTransactionsResponse.RequestStatusType Translate(
        this TransactionsPollResponse.RequestStatus fromBank)
    {
        return Enum.TryParse(typeof(OpenBankingTransactionsResponse.RequestStatusType), fromBank.ToString(),
            true, out var result)
            ? (OpenBankingTransactionsResponse.RequestStatusType)result
            : OpenBankingTransactionsResponse.RequestStatusType.Failed;
    }
}