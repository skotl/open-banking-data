﻿using OpenBanking.Models;

namespace OpenBanking.TrueLayer.EnumTranslators;

internal static class TransactionTypeEnumTranslator
{
    public static OpenBankingTransaction.TransactionTypeEnum Translate(string from)
    {
        if (string.Compare(from, OpenBankingTransaction.TransactionTypeEnum.Credit.ToString(),
                StringComparison.InvariantCultureIgnoreCase) == 0)
            return OpenBankingTransaction.TransactionTypeEnum.Credit;

        if (string.Compare(from, OpenBankingTransaction.TransactionTypeEnum.Debit.ToString(),
                StringComparison.InvariantCultureIgnoreCase) == 0)
            return OpenBankingTransaction.TransactionTypeEnum.Debit;

        throw new InvalidDataException($"Don't know how to decode Transaction Type={from}");
    }
}