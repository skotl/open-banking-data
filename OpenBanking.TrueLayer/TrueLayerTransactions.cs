using System.Net.Http.Headers;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Web;
using Microsoft.Extensions.Logging;
using OpenBanking.Abstract;
using OpenBanking.Abstract.Exceptions;
using OpenBanking.Models;
using OpenBanking.TrueLayer.EnumTranslators;
using OpenBanking.TrueLayer.Models;

namespace OpenBanking.TrueLayer;

public class TrueLayerTransactions(
    ILogger<TrueLayerTransactions> logger,
    IHttpClientFactory httpClientFactory)
    : IOpenBankingTransactions
{
    public async Task<string> RequestTransactionListAsync(string accountId, DateTime fromDate, DateTime toDate,
        string accessToken)
    {
        logger.LogDebug("Request transaction list for account#{AccountId}, {From}-{To}", accountId, fromDate, toDate);
        using var httpClient = httpClientFactory.CreateClient();
        using var request = new HttpRequestMessage(HttpMethod.Get, BuildRequestUri(accountId, fromDate, toDate));

        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

        var httpResponse = await httpClient.SendAsync(request);
        if (!httpResponse.IsSuccessStatusCode)
            throw new OpenBankingAccountsException(
                $"Account transactions request failed with response {(int)httpResponse.StatusCode}: {httpResponse.ReasonPhrase}");

        var json = await httpResponse.Content.ReadAsStringAsync();

        var jsonOptions = new JsonSerializerOptions();
        jsonOptions.Converters.Add(new JsonStringEnumConverter());
        var response = new TransactionsRequestResponse { Status = TransactionsRequestResponse.RequestStatus.Failed };
        if (!string.IsNullOrWhiteSpace(json))
            response = JsonSerializer.Deserialize<TransactionsRequestResponse>(json, jsonOptions);
        if (response is { Status: TransactionsRequestResponse.RequestStatus.Failed })
            throw new OpenBankingTransactionsRequestException("Unable to request transactions list");

        logger.LogDebug("Queued request for transactions, with poll uri={PollUri} and status={Status}",
            response?.ResultsUri, response?.Status);

        return response!.ResultsUri;
    }

    public async Task<OpenBankingTransactionsResponse> PollForTransactionResponseAsync(string accountId, string uri,
        string accessToken)
    {
        logger.LogDebug("Poll for transaction list for account#{AccountId} at {Uri}", accountId, uri);
        using var httpClient = httpClientFactory.CreateClient();
        using var request = new HttpRequestMessage(HttpMethod.Get, uri);

        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

        var httpResponse = await httpClient.SendAsync(request);
        if (!httpResponse.IsSuccessStatusCode)
            throw new OpenBankingAccountsException(
                $"Account transactions poll failed with response {(int)httpResponse.StatusCode}: {httpResponse.ReasonPhrase}");

        var json = await httpResponse.Content.ReadAsStringAsync();
        logger.LogDebug("JSON Response: {Json}", json);

        // If we get zero body back then there's not much else we can do than tell the caller we're queued
        if (string.IsNullOrWhiteSpace(json))
            return new OpenBankingTransactionsResponse
            {
                RequestStatus = OpenBankingTransactionsResponse.RequestStatusType.Queued,
                Transactions = []
            };

        var jsonOptions = new JsonSerializerOptions();
        jsonOptions.Converters.Add(new JsonStringEnumConverter());
        var response = JsonSerializer.Deserialize<TransactionsPollResponse>(json, jsonOptions);
        if (response?.Status == null)
            throw new OpenBankingPollTransactionsException(uri);

        logger.LogDebug("Polled transaction request for transactions, with poll uri={PollUri} and status={Status}",
            uri, response.Status);

        return Translate(response, accountId);
    }

    private static string BuildRequestUri(string accountId, DateTime fromDate, DateTime toDate)
    {
        var parms = new Dictionary<string, string?>
        {
            { "async", "true" },
            { "from", fromDate.ToString("s") },
            { "to", toDate.ToString("s") }
        };

        var uri = $"{Constants.TransactionsFetchUrl}/{accountId}/transactions";
        var query = HttpUtility.ParseQueryString("");
        parms.ToList().ForEach(kvp => query[kvp.Key] = kvp.Value);

        return string.Join("?", uri, query.ToString());
    }

    private static OpenBankingTransactionsResponse Translate(TransactionsPollResponse polledData, string accountId)
    {
        var response = new OpenBankingTransactionsResponse
        {
            RequestStatus = polledData.Status.Translate(),
            Transactions = []
        };

        response.Transactions.AddRange(polledData.Results!.Select(r => new OpenBankingTransaction
        {
            Updated = DateTime.UtcNow,
            AccountId = accountId,
            Amount = new OpenBankingMoney(r.Currency, r.Amount),
            RunningBalance = new OpenBankingMoney(
                r.RunningBalanceAmount.Currency,
                r.RunningBalanceAmount.Amount),
            Category = r.TransactionCategory,
            Classifications = r.TransactionClassification.ToList(),
            Description = r.Description,
            TransactionType = TransactionTypeEnumTranslator.Translate(r.TransactionType),
            Merchant = r.MerchantName,
            ProviderTransactionId = r.NormalisedProviderTransactionId,
            TimeStamp = r.TimeStamp
        }));

        return response;
    }
}
