﻿using System.Net.Http.Headers;
using System.Net.Http.Json;
using Microsoft.Extensions.Logging;
using OpenBanking.Abstract;
using OpenBanking.Abstract.Exceptions;
using OpenBanking.Models;
using OpenBanking.TrueLayer.Models;

namespace OpenBanking.TrueLayer;

public class TrueLayerAccounts(
    ILogger<TrueLayerAccounts> logger,
    IHttpClientFactory httpClientFactory)
    : IOpenBankingAccounts
{
    public async Task<List<OpenBankingAccount>> GetAccountListAsync(string accessToken, string userId)
    {
        logger.LogInformation("Get account list for {UserId}", userId);
        using var httpClient = httpClientFactory.CreateClient();
        using var request = new HttpRequestMessage(HttpMethod.Get, Constants.AccountsListUrl);

        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

        var httpResponse = await httpClient.SendAsync(request);
        if (!httpResponse.IsSuccessStatusCode)
            throw new OpenBankingAccountsException(
                $"Accounts list failed with response {(int)httpResponse.StatusCode}: {httpResponse.ReasonPhrase}");

        var response = await httpResponse.Content.ReadFromJsonAsync<AccountsListResponse>();
        if (response is not { Status: Constants.SucceededStatus })
            throw new OpenBankingAccountsException("Unable to retrieve accounts list");

        logger.LogInformation("Retrieved {AccountNumber} accounts for {UserId}", response.Accounts.Length, userId);

        return DtoToModel(response);
    }

    private static List<OpenBankingAccount> DtoToModel(AccountsListResponse response)
    {
        return response.Accounts.Select(account => 
                new OpenBankingAccount
            {
                AccountId = account.AccountId ?? "",
                Currency = account.Currency ?? "",
                Updated = account.UpdateTimestamp,
                AccountNumber = account.AccountNumber?.Number,
                AccountType = account.AccountType ?? "",
                DisplayName = account.DisplayName ?? "",
                LogoUri = account.Provider?.LogoUri,
                ProviderId = account.Provider?.ProviderId ?? "",
                ProviderName = account.Provider?.DisplayName ?? "",
                SortCode = account.AccountNumber?.SortCode ?? ""
            })
            .ToList();
    }
}