﻿namespace OpenBanking.TrueLayer;

internal static class Constants
{
    public const string AuthenticationUrl = "https://auth.truelayer.com/connect/token";
    public const string RefreshAccessTokenUrl = "https://auth.truelayer.com/connect/token";
    public const string IdentityUrl = "https://api.truelayer.com/data/v1/info";
    public const string AccountsListUrl = "https://api.truelayer.com/data/v1/accounts";
    public const string TransactionsFetchUrl = "https://api.truelayer.com/data/v1/accounts";

    public const string SucceededStatus = "Succeeded";
}
