﻿using System.Net.Http.Headers;
using System.Net.Http.Json;
using Microsoft.Extensions.Logging;
using OpenBanking.Abstract;
using OpenBanking.Abstract.Exceptions;
using OpenBanking.TrueLayer.Models;

namespace OpenBanking.TrueLayer;

public class TrueLayerIdentity(
    ILogger<TrueLayerIdentity> logger,
    IHttpClientFactory httpClientFactory)
    : IOpenBankingIdentity
{
    public async Task<string> GetIdentityAsync(string accessToken, string userId)
    {
        logger.LogInformation("Fetching identity for {UserId}", userId);
        
        using var httpClient = httpClientFactory.CreateClient();
        using var request = new HttpRequestMessage(HttpMethod.Get, Constants.IdentityUrl);

        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

        var httpResponse = await httpClient.SendAsync(request);
        if (!httpResponse.IsSuccessStatusCode)
            throw new OpenBankingIdentityException(
                $"Fetch identity failed with response {(int)httpResponse.StatusCode}: {httpResponse.ReasonPhrase}");

        var response = await httpResponse.Content.ReadFromJsonAsync<IdentityResponse>();
        if (response == null || response.Results.Length == 0)
            throw new OpenBankingIdentityException("Unable to retrieve identity");

        var fullName = response.Results[0].FullName;
        logger.LogInformation("Got identity as '{FullName}' for {UserId}", fullName, userId);
        
        return fullName;
    }
}