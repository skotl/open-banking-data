﻿using System.Text.Json.Serialization;

namespace OpenBanking.TrueLayer.Models;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable once ClassNeverInstantiated.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by json serialisation
public class IdentityResponse
{
    // ReSharper disable once ClassNeverInstantiated.Global
    // Reason: created by json serializer
    public class InnerResult
    {
        [JsonPropertyName("full_name")] public string FullName { get; set; } = string.Empty;

        [JsonPropertyName("update_timestamp")] public DateTime UpdateTimestamp { get; set; } = DateTime.MinValue;
    }

    public InnerResult[] Results { get; set; } = Array.Empty<InnerResult>();
}