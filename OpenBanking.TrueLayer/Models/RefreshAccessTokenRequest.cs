﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace OpenBanking.TrueLayer.Models;

// ReSharper disable UnusedMember.Global
// ReSharper disable ClassNeverInstantiated.Global
// Reason: JSON instantiated
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Json serialized")]
[SuppressMessage("ReSharper", "PropertyCanBeMadeInitOnly.Global", Justification = "Json serialized")]
[SuppressMessage("Performance", "CA1822:Mark members as static")]
internal class RefreshAccessTokenRequest
{
    [JsonPropertyName("grant_type")] 
    public string GrantType => "refresh_token";

    [JsonPropertyName("client_id")]
    public string ClientId { get; set; }  = string.Empty;
    
    [JsonPropertyName("client_secret")]
    public string ClientSecret { get; set; } = string.Empty;
    
    [JsonPropertyName("refresh_token")]
    public string RefreshToken { get; set; } = string.Empty;
  
}