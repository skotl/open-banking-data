﻿using System.Text.Json.Serialization;

namespace OpenBanking.TrueLayer.Models;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by JSON serialisation

public class TransactionsPollResponse
{
    
    public enum RequestStatus
    {
        Queued,
        Running,
        Succeeded,
        Failed
    }
    
    [JsonPropertyName("status")]
    public RequestStatus Status { get; set; }
    
    [JsonPropertyName("results")]
    public List<TransactionResponse>? Results { get; set; }
}