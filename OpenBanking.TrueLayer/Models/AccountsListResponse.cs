﻿using System.Text.Json.Serialization;

namespace OpenBanking.TrueLayer.Models;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Global
// ReSharper disable ClassNeverInstantiated.Global
// Reason: Json serialized
internal class AccountsListResponse
{
    public class AccountNumber
    {
        [JsonPropertyName("iban")]
        public string? IBAN { get; set; }
        
        [JsonPropertyName("swift_bic")]
        public string? SwiftBIC { get; set; }
        
        [JsonPropertyName("number")]
        public string? Number { get; set; }
        
        [JsonPropertyName("sort_code")]
        public string? SortCode { get; set; }
    }

    public class AccountProvider
    {
        [JsonPropertyName("display_name")]
        public string? DisplayName { get; set; }

        [JsonPropertyName("provider_id")]
        public string? ProviderId { get; set; }

        [JsonPropertyName("logo_uri")]
        public string? LogoUri { get; set; }
    }

    public class Account
    {
        [JsonPropertyName("update_timestamp")]
        public DateTime UpdateTimestamp { get; set; }

        [JsonPropertyName("account_id")]
        public string? AccountId { get; set; }

        [JsonPropertyName("account_type")]
        public string? AccountType { get; set; }

        [JsonPropertyName("display_name")]
        public string? DisplayName { get; set; }

        [JsonPropertyName("currency")]
        public string? Currency { get; set; }
        
        [JsonPropertyName("account_number")] 
        public AccountNumber? AccountNumber { get; set; }
        
        [JsonPropertyName("provider")]
        public AccountProvider? Provider { get; set; }
    }

    [JsonPropertyName("results")] 
    public Account[] Accounts { get; set; } = Array.Empty<Account>();
    
    [JsonPropertyName("status")]
    public string? Status { get; set; }
}