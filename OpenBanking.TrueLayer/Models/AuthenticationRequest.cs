﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace OpenBanking.TrueLayer.Models;

// ReSharper disable UnusedMember.Global
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Json serialized")]
[SuppressMessage("ReSharper", "PropertyCanBeMadeInitOnly.Global", Justification = "Json serialized")]
[SuppressMessage("Performance", "CA1822:Mark members as static")]
internal class AuthenticationRequest
{
    [JsonPropertyName("grant_type")] 
    public string GrantType => "authorization_code";

    [JsonPropertyName("client_id")]
    public string ClientId { get; set; }  = string.Empty;
    
    [JsonPropertyName("client_secret")]
    public string ClientSecret { get; set; } = string.Empty;
    
    [JsonPropertyName("redirect_uri")]
    public string RedirectUri { get; set; } = string.Empty;
    
    [JsonPropertyName("code")]
    public string Code { get; set; } = string.Empty;
}