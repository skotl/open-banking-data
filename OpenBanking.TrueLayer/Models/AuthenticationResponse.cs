﻿using System.Text.Json.Serialization;

namespace OpenBanking.TrueLayer.Models;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable once ClassNeverInstantiated.Global
// Reason: created by json serializer
internal class AuthenticationResponse
{
    [JsonPropertyName("access_token")] public string AccessToken { get; set; } = string.Empty;

    [JsonPropertyName("expires_in")] public int ExpiresIn { get; set; } = 0;

    [JsonPropertyName("token_type")] public string TokenType { get; set; }  = string.Empty;

    [JsonPropertyName("refresh_token")] public string RefreshToken { get; set; } = string.Empty;
}