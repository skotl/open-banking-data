﻿using System.Text.Json.Serialization;

namespace OpenBanking.TrueLayer.Models;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by json serialisation
public class TransactionResponse
{
    public class RunningBalance
    {
        [JsonPropertyName("currency")]
        public string Currency { get; set; } = string.Empty;
        [JsonPropertyName("amount")]
        public decimal Amount { get; set; }
    }
    
    [JsonPropertyName("transaction_id")]
    public string TransactionId { get; set; } = string.Empty;
    
    [JsonPropertyName("normalised_provider_transaction_id")]
    public string NormalisedProviderTransactionId { get; set; } = string.Empty;
    
    [JsonPropertyName("provider_transaction_id")]
    public string ProviderTransactionId { get; set; } = string.Empty;
    
    [JsonPropertyName("timestamp")]
    public DateTime TimeStamp { get; set; }
    
    [JsonPropertyName("description")]
    public string Description { get; set; } = string.Empty;
    
    [JsonPropertyName("transaction_type")]
    public string TransactionType { get; set; } = string.Empty;
    
    [JsonPropertyName("transaction_category")]
    public string TransactionCategory { get; set; } = string.Empty;
    
    [JsonPropertyName("transaction_classification")]
    public string[] TransactionClassification { get; set; } = [];
    
    [JsonPropertyName("merchant_name")]
    public string MerchantName { get; set; } = string.Empty;
    
    [JsonPropertyName("amount")]
    public decimal Amount { get; set; }
    
    [JsonPropertyName("currency")]
    public string Currency { get; set; } = string.Empty;

    [JsonPropertyName("running_balance")]
    public RunningBalance RunningBalanceAmount { get; set; } = null!;
}