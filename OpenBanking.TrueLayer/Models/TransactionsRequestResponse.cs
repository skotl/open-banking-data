﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace OpenBanking.TrueLayer.Models;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by json serialisation

internal class TransactionsRequestResponse
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    // Reason: Set by json serialisation
    public enum RequestStatus
    {

        Queued,
        Running,
        Succeeded,
        Failed
    }

    [JsonPropertyName("results_uri")]
    public string ResultsUri { get; set; } = string.Empty;
    
    [JsonPropertyName("status")]
    public RequestStatus Status { get; set; }

    [JsonPropertyName("task_id")]
    public string TaskId { get; set; } = string.Empty;
}