﻿using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Web;
using Microsoft.Extensions.Logging;
using OpenBanking.Abstract;
using OpenBanking.Abstract.Exceptions;
using OpenBanking.Models;
using OpenBanking.TrueLayer.Models;

namespace OpenBanking.TrueLayer;

public class TrueLayerAuthentication(
    ILogger<TrueLayerAuthentication> logger,
    IHttpClientFactory httpClientFactory)
    : IOpenBankingAuthentication
{
    public string BuildAuthenticationUri(string host, string clientId, string redirectUri, IEnumerable<string> scopes, IEnumerable<string> providers)
    {
        var uri = new UriBuilder("https", host);
        var query = HttpUtility.ParseQueryString(uri.Query);
        query["response_type"] = "code";
        query["client_id"] = clientId;
        query["redirect_uri"] = redirectUri;
        query["scope"] = AsString(scopes);
        query["providers"] = AsString(providers);

        uri.Query = query.ToString();
        
        logger.LogDebug("Auth Uri: {Uri}", uri);
        return uri.ToString();
    }

    public async Task<Jwt> ConvertCodeAsync(string userId, string clientId, string clientSecret, string redirectUrl, string code)
    {
        logger.LogDebug("Converting code to JWT for user {UserId}", userId);
        
        using var httpClient = httpClientFactory.CreateClient();
        var requestConfig = new AuthenticationRequest
        {
            ClientId = clientId, ClientSecret = clientSecret, Code = code, RedirectUri = redirectUrl
        };
        
        var requestJson = new StringContent(JsonSerializer.Serialize(requestConfig),
            Encoding.UTF8,
            MediaTypeNames.Application.Json);

        using var httpResponse = await httpClient.PostAsync(Constants.AuthenticationUrl, requestJson);
        if (!httpResponse.IsSuccessStatusCode)
            throw new OpenBankingAuthenticationException(
                $"Authentication failed with response {(int)httpResponse.StatusCode}: {httpResponse.ReasonPhrase}");

        var response = await httpResponse.Content.ReadFromJsonAsync<AuthenticationResponse>();
        if (response == null)
            throw new OpenBankingAuthenticationException($"Unable to retrieve token for user {userId}");
        
        return new Jwt
        {
            AccessToken = response.AccessToken, ExpiryUtc = DateTime.UtcNow.AddSeconds(response.ExpiresIn),
            RefreshToken = response.RefreshToken, TokenType = response.TokenType
        };
    }

    public async Task<Jwt> RefreshAccessTokenAsync(string userId, string clientId, string clientSecret, string refreshToken)
    {
        logger.LogDebug("Requesting refreshed access token for user {UserId}", userId);
        
        using var httpClient = httpClientFactory.CreateClient();
        var requestConfig = new RefreshAccessTokenRequest
        {
            ClientId = clientId, ClientSecret = clientSecret, RefreshToken = refreshToken
        };
        
        var requestJson = new StringContent(JsonSerializer.Serialize(requestConfig),
            Encoding.UTF8,
            MediaTypeNames.Application.Json);

        using var httpResponse = await httpClient.PostAsync(Constants.RefreshAccessTokenUrl, requestJson);
        if (!httpResponse.IsSuccessStatusCode)
            throw new OpenBankingTokenRefreshException(
                $"Refresh token exchange failed with response {(int)httpResponse.StatusCode}: {httpResponse.ReasonPhrase}");
        
        var response = await httpResponse.Content.ReadFromJsonAsync<AuthenticationResponse>();
        if (response == null)
            throw new OpenBankingAuthenticationException($"Unable to retrieve token for user {userId}");
        
        return new Jwt
        {
            AccessToken = response.AccessToken, ExpiryUtc = DateTime.UtcNow.AddSeconds(response.ExpiresIn),
            RefreshToken = response.RefreshToken, TokenType = response.TokenType
        };
    }

    private static string AsString(IEnumerable<string> vars)
    {
        var sb = new StringBuilder();
        foreach (var v in vars)
        {
            if (sb.Length > 0)
                sb.Append(' ');
            sb.Append(v);
        }

        return sb.ToString();
    }
}