using OpenBanking.Abstract;
using OpenBanking.Models;
using OpenBanking.Storage;
using OpenBanking.TrueLayer;
using Serilog;
using OpenBanking.ScheduledTasks;
using OpenBanking.WorkerService.QueuedTasks;
using OpenBanking.WorkerService.TaskListManagement;

// Get an IsDevelopment flag before we've executed builder.build()
var isDevelopment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

var builder = Host.CreateApplicationBuilder(args);
builder.Services.AddHostedService<QueueController>();

// Serilog
builder.Services.AddSerilog((_, lc) => lc
    .WriteTo.Console()
    .ReadFrom.Configuration(builder.Configuration).Enrich.WithThreadId()
);

// Set options
builder.Services.Configure<OpenBankingConfiguration>(
    builder.Configuration.GetSection(OpenBankingConfiguration.Section));

// DI setup
builder.Services.AddScoped<IOpenBankingAuthentication, TrueLayerAuthentication>();
builder.Services.AddScoped<IOpenBankingIdentity, TrueLayerIdentity>();
builder.Services.AddScoped<IOpenBankingAccounts, TrueLayerAccounts>();
builder.Services.AddScoped<IOpenBankingTransactions, TrueLayerTransactions>();

builder.Services.AddHttpClient();

builder.Services.AddDatabaseStorage(builder.Configuration.GetConnectionString("DefaultConnection") ?? "not set", isDevelopment);
builder.Services.AddScheduleTaskSupport();

// Local services
builder.Services.AddSingleton<ITaskDispatcher, TaskDispatcher>();
builder.Services.AddSingleton<ITaskList, TaskList>();

var host = builder.Build();
host.Run();