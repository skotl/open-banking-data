﻿namespace OpenBanking.WorkerService.TaskListManagement;

internal class TaskList(ILogger<TaskList> logger) : ITaskList
{
    private readonly Dictionary<Task, Guid> _runningTasks = [];

    public int RunningTaskCount => _runningTasks.Count;

    public void Add(Task task, Guid taskId)
    {
        _runningTasks.Add(task, taskId);
    }

    public void WaitAll()
    {
        Task.WaitAll(_runningTasks.Select(t => t.Key).ToArray());
    }

    public void ClearCompletedTasks()
    {
        logger.LogDebug("Cleaning {Count} running tasks", RunningTaskCount);

        var completedTasks = _runningTasks
            .Select(tp => tp.Key)
            .Where(tp => tp.IsCompleted)
            .ToList();

        completedTasks.ForEach(t => _runningTasks.Remove(t));

        logger.LogDebug("Now have {Count} running tasks", RunningTaskCount);
    }
}