﻿namespace OpenBanking.WorkerService.TaskListManagement;

public interface ITaskList
{
    int RunningTaskCount { get; }
    void Add(Task task, Guid taskId);
    void WaitAll();
    void ClearCompletedTasks();
}