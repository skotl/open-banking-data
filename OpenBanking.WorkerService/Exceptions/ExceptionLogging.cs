﻿using System.Text;
using OpenBanking.Models;

namespace OpenBanking.WorkerService.Exceptions;

public static class ExceptionLogging
{
    public static string Prettify(this Exception? ex, OpenBankingScheduledTask? task = null)
    {
        var sb = new StringBuilder();
        var stackTrace = ex?.StackTrace ?? null;

        if (task != null)
            sb.AppendLine($"{ex?.GetType().Name} in task #{task.Id}, {task.Description}");

        while (ex != null)
        {
            sb.AppendLine($"{ex.Message} ({ex.GetType().Namespace}");

            ex = ex.InnerException;
        }

        sb.AppendLine(stackTrace);

        return sb.ToString();
    }
}