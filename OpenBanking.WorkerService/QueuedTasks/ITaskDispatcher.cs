﻿using OpenBanking.Models;

namespace OpenBanking.WorkerService.QueuedTasks;

internal interface ITaskDispatcher
{
    Task<TaskCompletionStatus> DispatchAsync(OpenBankingScheduledTask task, CancellationToken stoppingToken);
}