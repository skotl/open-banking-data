using OpenBanking.Storage.Repositories;
using OpenBanking.WorkerService.Exceptions;
using OpenBanking.WorkerService.TaskListManagement;

namespace OpenBanking.WorkerService.QueuedTasks;

internal class QueueController(
    ILogger<QueueController> logger,
    IServiceProvider serviceProvider,
    ITaskDispatcher taskDispatcher,
    ITaskList taskList)
    : BackgroundService
{
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        logger.LogInformation("Worker started");

        using (var scope = serviceProvider.CreateScope())
        {
            var scheduledTaskRepository = scope.ServiceProvider.GetRequiredService<IScheduledTaskRepository>();

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var tasks = await scheduledTaskRepository.GetOutstandingJobsAsync();
                    foreach (var task in tasks)
                    {
                        taskList.Add(
                            Task.Run(async () =>
                                {
                                    await scheduledTaskRepository.MarkJobInProgressAsync(task.Id);
                                    var response = await taskDispatcher.DispatchAsync(task, stoppingToken);
                                    await CleanupAfterJob(task.Id, response.WasSuccessful, response.Message);
                                },
                                stoppingToken), task.Id);
                    }

                    await Task.Delay(1000, stoppingToken);

                    taskList.ClearCompletedTasks();
                }
                catch (Exception ex)
                {
                    logger.LogError("{Error}", ex.Prettify());
                }
            }
        }

        logger.LogInformation("Waiting for {Count} tasks to complete", taskList.RunningTaskCount);

        taskList.WaitAll();

        logger.LogInformation("Worker ending");
    }


    private async Task CleanupAfterJob(Guid taskId, bool success, string message)
    {
        logger.LogInformation("Task {TaskId} completed {Status}", taskId, success ? "successfully" : "with failure");

        using var scope = serviceProvider.CreateScope();
        var scheduledTaskRepository = scope.ServiceProvider.GetRequiredService<IScheduledTaskRepository>();

        await scheduledTaskRepository.ClearJobInProgressAsync(taskId);

        if (success)
            await scheduledTaskRepository.RemoveIfNotDeferred(taskId, message);
        else
            await scheduledTaskRepository.RemoveErroredJob(taskId, message);
    }
}