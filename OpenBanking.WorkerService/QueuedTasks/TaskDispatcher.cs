﻿using OpenBanking.Models;
using OpenBanking.ScheduledTasks.Orchestration;
using OpenBanking.WorkerService.Exceptions;

namespace OpenBanking.WorkerService.QueuedTasks;

internal class TaskDispatcher(ILogger<TaskDispatcher> logger, IServiceProvider serviceProvider)
    : ITaskDispatcher
{
    public async Task<TaskCompletionStatus> DispatchAsync(OpenBankingScheduledTask task, CancellationToken stoppingToken)
    {
        var completionStatus = new TaskCompletionStatus();

        try
        {
            await ExecuteAsync(task, stoppingToken);
        }
        catch (Exception ex)
        {
            completionStatus.SetFailed(ex.Prettify(task));
            logger.LogError("{Error}", completionStatus.Message);
        }

        return completionStatus;
    }

    // ReSharper disable once UnusedParameter.Local
    // Reason: TODO: Should probably pass cancellationToken to the dispatched client
    private async Task ExecuteAsync(OpenBankingScheduledTask task, CancellationToken cancellationToken)
    {
        using var scope = serviceProvider.CreateScope();

        logger.LogInformation("Starting task with ID={TaskId}, of type {Type}: {Desc}",
            task.Id, task.TaskType, task.Description);

        var dispatcher = scope.ServiceProvider.GetRequiredService<IJobDispatcher>();
        await dispatcher.DispatchAsync(task);

        logger.LogInformation("Task dispatcher thread for ID={TaskId} ended, {Desc}", task.Id, task.Description);
    }
}