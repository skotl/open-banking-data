﻿namespace OpenBanking.WorkerService.QueuedTasks;

internal class TaskCompletionStatus
{
    public bool WasSuccessful { get; private set; } = true;
    public string Message { get; private set; } = "Completed successfully";

    public void SetFailed(string message)
    {
        WasSuccessful = false;
        Message = message;
    }
}