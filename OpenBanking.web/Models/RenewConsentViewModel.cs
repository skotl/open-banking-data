﻿namespace OpenBanking.Web.Models;

public class RenewConsentViewModel
{
    public Guid ConnectionId { get; init; }
    public string UserFullName { get; init; } = string.Empty;
    public string Provider { get; init; } = string.Empty;
}