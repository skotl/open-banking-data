﻿namespace OpenBanking.Web.Models;

public class HomePageViewModel
{
    public string OpenBankingAuthenticateUri { get; init; } = string.Empty;

    public List<BankConnectionViewModel> BankConnections { get; init; } = [];
}
