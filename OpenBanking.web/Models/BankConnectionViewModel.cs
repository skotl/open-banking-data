﻿namespace OpenBanking.Web.Models;

public class BankConnectionViewModel
{
    public Guid Id { get; init; }

    public string Provider { get; init; } = string.Empty;

    public string? ProviderLogo { get; init; } = string.Empty;

    public DateTime ConsentExpires { get; init; }

    public List<AccountHeaderViewModel> Accounts { get; } = [];
}