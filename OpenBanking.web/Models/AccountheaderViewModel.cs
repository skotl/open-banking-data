﻿namespace OpenBanking.Web.Models;

public class AccountHeaderViewModel
{
    public Guid Id { get; init; }
    public string Bank { get; init; } = string.Empty;
    public string DisplayName { get; init; } = string.Empty;
    public string SortCode { get; init; } = string.Empty;
    public string AccountNumber { get; init; } = string.Empty;
    public string Currency { get; init; } = string.Empty;
}