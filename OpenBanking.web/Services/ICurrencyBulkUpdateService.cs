﻿namespace OpenBanking.Web.Services;

public interface ICurrencyBulkUpdateService
{
    Task LoadAsync(string filename);
}