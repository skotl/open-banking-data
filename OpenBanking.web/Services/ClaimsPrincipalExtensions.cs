﻿using System.Security.Claims;

namespace OpenBanking.Web.Services;

public static class ClaimsPrincipalExtensions
{
    /// <summary>
    /// Gets the user's SID from its list of claims
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    /// <exception cref="InvalidDataException">If there is no claim for the SID</exception>
    public static string GetIdentity(this ClaimsPrincipal user)
    {
        return user.FindFirst(ClaimTypes.NameIdentifier)?.Value
               ?? throw new InvalidDataException("Name Identify not found in claims");
    }
}