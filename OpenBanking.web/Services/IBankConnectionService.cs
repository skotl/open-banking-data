﻿using OpenBanking.Models;

namespace OpenBanking.Web.Services;

public interface IBankConnectionService
{
    Task PopulateBankAccountsAsync(string code, string userId);
    Task<List<OpenBankingAccount>> GetAccountListTestingAsync(Guid accountId, string userId);
}