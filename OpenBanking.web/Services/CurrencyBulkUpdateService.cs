﻿using OpenBanking.Models;
using OpenBanking.Storage.Repositories;

namespace OpenBanking.Web.Services;

public class CurrencyBulkUpdateService(
    ILogger<CurrencyBulkUpdateService> logger,
    ICurrencyRepository currencyRepository,
    IHttpClientFactory httpClientFactory)
    : ICurrencyBulkUpdateService
{
    public async Task LoadAsync(string filename)
    {
        if (string.IsNullOrWhiteSpace(filename))
            throw new ArgumentException(null, nameof(filename));
        if (!File.Exists(filename))
            throw new FileNotFoundException(filename);
        
        logger.LogDebug("Bulk load currency data from {FileName}", filename);

        using var reader = File.OpenText(filename);
        while (await ReadCurrency(reader) is { } currency)
        {
            await currencyRepository.InsertOrUpdateAsync(currency);
        }
    }

    // Expects the file to have five lines for:
    //      flag url
    //      Region
    //      Denomination
    //      ISO Code
    //      Symbol
    // ... plus a blank line
    private async Task<CurrencyDefinition?> ReadCurrency(TextReader reader)
    {
        var lines = new string?[6];
        for(var i=0; i<6; i++)
        {
            lines[i] = await reader.ReadLineAsync();
        }

        if (lines[0] == null)
            return null;

        return new CurrencyDefinition
        {
            Flag = await GetImageFromUrl(lines[0]),
            Region = lines[1]!,
            Name = lines[2]!,
            Code = lines[3]!,
            Symbol = lines[4]!
        };
    }

    private async Task<byte[]> GetImageFromUrl(string? url)
    {
        var httpClient = httpClientFactory.CreateClient();

        await using var httpStream = await httpClient.GetStreamAsync(url);
        using var memoryStream = new MemoryStream();
        await httpStream.CopyToAsync(memoryStream);
        return memoryStream.ToArray();
    }
}