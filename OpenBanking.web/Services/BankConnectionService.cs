﻿using Microsoft.Extensions.Options;
using OpenBanking.Abstract;
using OpenBanking.Models;
using OpenBanking.ScheduledTasks.PublicServices;
using OpenBanking.Storage.Repositories;

namespace OpenBanking.Web.Services;

public class BankConnectionService(
    ILogger<BankConnectionService> logger,
    IOptions<OpenBankingConfiguration> options,
    IGenerateHistoricTransactionsRequestService generateHistoricTransactionsRequestService,
    IOpenBankingAuthentication openBankingAuthentication,
    IOpenBankingIdentity openBankingIdentity,
    IOpenBankingAccounts openBankingAccounts,
    IBankingProviderRepository bankingProviderRepository,
    IBankConsentRepository bankConsentRepository,
    IBankAccountRepository bankAccountRepository)
    : IBankConnectionService
{
    private readonly OpenBankingConfiguration _options = options.Value;

    public async Task PopulateBankAccountsAsync(string code, string userId)
    {
        logger.LogInformation("Populate bank accounts for {UserId}", userId);

        var jwt = await openBankingAuthentication.ConvertCodeAsync(userId, _options.ClientId, _options.ClientSecret,
            _options.RedirectUri, code);

        var fullName = await openBankingIdentity.GetIdentityAsync(jwt.AccessToken, userId);
        var accounts = await openBankingAccounts.GetAccountListAsync(jwt.AccessToken, userId);

        var bankingProviderId = GetProviderId(accounts);
        var bankingProviderName = GetProviderName(accounts);
        var bankingProviderLogo = GetProviderLogo(accounts);
        var providerId = await bankingProviderRepository.CreateOrUpdateAsync(
                bankingProviderId, bankingProviderName, bankingProviderLogo);

        var connection = await CreateOrUpdateBankConnectionRecordAsync(providerId, userId, jwt, fullName, accounts);

        var accountIds = await bankAccountRepository.AddOrUpdateAsync(connection.Consent.Id, accounts);
        accountIds.ForEach(GenerateHistoricRequests);

        logger.LogInformation("Returning user details for {UserId}", userId);
    }

    public async Task<List<OpenBankingAccount>> GetAccountListTestingAsync(Guid accountId, string userId)
    {
        var accessToken = await bankConsentRepository.GetAccessTokenForBankAccount(accountId);

        return await openBankingAccounts.GetAccountListAsync(accessToken, userId);
    }

    private async void GenerateHistoricRequests(Guid id)
    {
        await generateHistoricTransactionsRequestService.GenerateRequestsAsync(id);
    }

    private static OpenBankingConnection BuildBankConnectionRecord(
        Jwt jwt, string fullName, List<OpenBankingAccount> accounts, OpenBankingConsent? consent)
    {
        consent ??= new OpenBankingConsent();
        consent.AccessToken = jwt.AccessToken;
        consent.RefreshToken = jwt.RefreshToken;
        consent.ConsentGranted = DateTime.UtcNow;
        consent.RefreshTokenExpires = DateTime.UtcNow.AddDays(30);
        consent.TokenExpiryUtc = jwt.ExpiryUtc;

        return new OpenBankingConnection
        {
            Accounts = accounts,
            Provider = GetProviderName(accounts), ProviderLogoUri = GetProviderLogo(accounts), UserFullName = fullName,
            Consent = consent
        };
    }

    private async Task<OpenBankingConnection> CreateOrUpdateBankConnectionRecordAsync(Guid providerId, string userId,
        Jwt jwt, string fullName, List<OpenBankingAccount> accounts)
    {
        var existingConsent = await bankConsentRepository.GetForProviderAsync(userId, providerId);
        var consentAlreadyExisted = existingConsent != default;
        var connection = BuildBankConnectionRecord(jwt, fullName, accounts, existingConsent);

        if (connection.Consent == default)
        {
            throw new InvalidDataException("Missing Consent object on the connection");
        }

        // If we didn't have an existing consent then the structure was created in the bank connection,
        // so create that consent record and add the bank accounts
        if (!consentAlreadyExisted)
        {
            await bankConsentRepository.Add(providerId, userId, connection);
        }
        else // Otherwise update the existing consent record with the new JWT values
        {
            connection.Consent.Id = existingConsent!.Id;
            await bankConsentRepository.UpdateAsync(connection.Consent);
        }

        return connection;
    }

    private static string GetProviderLogo(IReadOnlyList<OpenBankingAccount> accounts)
    {
        if (accounts.Count == 0)
            return "";

        return !string.IsNullOrWhiteSpace(accounts[0].LogoUri)
            ? accounts[0].LogoUri!
            : "";
    }

    private static string GetProviderName(IReadOnlyList<OpenBankingAccount> accounts)
    {
        if (accounts.Count == 0)
            return "unknown";

        if (!string.IsNullOrWhiteSpace(accounts[0].ProviderName))
            return accounts[0].ProviderName!;

        return accounts[0].ProviderId ?? "unknown";
    }

    private static string GetProviderId(IReadOnlyList<OpenBankingAccount> accounts)
    {
        if (accounts.Count == 0)
            return "unknown";

        return !string.IsNullOrWhiteSpace(accounts[0].ProviderId)
            ? accounts[0].ProviderId!
            : "unknown";
    }
}