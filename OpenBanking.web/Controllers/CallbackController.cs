﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OpenBanking.Web.Services;

namespace OpenBanking.Web.Controllers;

[Authorize]
public class CallbackController(
    ILogger<CallbackController> logger,
    IBankConnectionService bankConnectionService)
    : Controller
{
    // GET
    public async Task<IActionResult> Index(string code)
    {
        logger.LogInformation("Received callback");

        if (string.IsNullOrWhiteSpace(code))
            throw new ArgumentException("Expected code");

        var sid = User.GetIdentity();

        await bankConnectionService.PopulateBankAccountsAsync(code, sid);
        
        return RedirectToAction("Index", "Home");
    }

   
}