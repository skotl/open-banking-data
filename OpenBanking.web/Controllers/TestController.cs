﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OpenBanking.ScheduledTasks.PublicServices;
using OpenBanking.Storage.Repositories;
using OpenBanking.Web.Services;

namespace OpenBanking.Web.Controllers;

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously

[Authorize]
public class TestController(
    IGenerateHistoricTransactionsRequestService generateHistoricTransactionsRequestService,
    IGenerateAccountTransactionsRequestService generateAccountTransactionsRequestService,
    IDestroyAllScheduledJobsService destroyAllScheduledJobsService,
    IBankConsentRepository bankConsentRepository,
    IBankConnectionService bankConnectionService,
    ICurrencyBulkUpdateService currencyBulkUpdateService,
    ICurrencyRepository currencyRepository)
    : Controller
{
    public async Task<IActionResult> IndexAsync()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> RequestAccountsList(Guid accountId)
    {
        var sid = User.GetIdentity();

        var accounts = await bankConnectionService.GetAccountListTestingAsync(accountId, sid);

        return Ok(accounts);
    }

    [HttpPost]
    public async Task<IActionResult> RequestTransactions(Guid accountId, DateTime from, DateTime to)
    {
        await generateAccountTransactionsRequestService.GenerateAsync(accountId, from, to);

        return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> RequestHistoricTransactions(Guid accountId)
    {
        await generateHistoricTransactionsRequestService.GenerateRequestsAsync(accountId);

        return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> DeleteAllJobs()
    {
        await destroyAllScheduledJobsService.ExecuteAsync();

        return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> GetBearer(Guid accountId)
    {
        return new OkObjectResult(new { bearer = await bankConsentRepository.GetAccessTokenForBankAccount(accountId) });
    }

    [HttpPost]
    public async Task<IActionResult> LoadCurrencies()
    {
        await currencyBulkUpdateService.LoadAsync("currencies-list.txt");

        return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> GetCurrencies(bool getFlags)
    {
        return new OkObjectResult(await currencyRepository.GetAllAsync(getFlags));
    }
}