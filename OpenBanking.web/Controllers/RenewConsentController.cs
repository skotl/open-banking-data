﻿using Microsoft.AspNetCore.Mvc;
using OpenBanking.Storage.Repositories;
using OpenBanking.Web.Models;
using OpenBanking.Web.Services;

namespace OpenBanking.Web.Controllers;

public class RenewConsentController(IUserRepository userRepository) : Controller
{
    // GET
    public async Task<IActionResult> Index(Guid id)
    {
        var sid = User.GetIdentity();
        var user = await userRepository.GetAsync(sid);

        if (user == null)
            throw new InvalidDataException("user");

        var connection = user.BankConnections.FirstOrDefault(c => c.Id == id);
        if (connection == null)
            throw new InvalidDataException("connection");

        return View(new RenewConsentViewModel
        {
            ConnectionId = id,
            UserFullName = connection.UserFullName,
            Provider = connection.Provider ?? "unknown"
        });
    }
}