﻿using System.Diagnostics;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OpenBanking.Abstract;
using OpenBanking.Models;
using OpenBanking.Storage.Repositories;
using OpenBanking.Web.Models;
using OpenBanking.Web.Services;

namespace OpenBanking.Web.Controllers;

[Authorize]
public class HomeController(
    ILogger<HomeController> logger,
    IOptions<OpenBankingConfiguration> options,
    IOpenBankingAuthentication openBankingAuthentication,
    IUserRepository userRepository,
    IBankConsentRepository bankConsentRepository)
    : Controller
{
    private readonly OpenBankingConfiguration _options = options.Value;

    public async Task<IActionResult> IndexAsync()
    {
        var authUri = openBankingAuthentication.BuildAuthenticationUri(_options.Host, _options.ClientId,
            _options.RedirectUri, _options.Scopes, _options.Providers);

        var sid = User.GetIdentity();
        await userRepository.CreateIfNotExistsAsync(sid);

        var userDetails = await userRepository.GetAsync(sid);

        return View(BuildVm(userDetails, authUri));
    }

    [HttpPost]
    public async Task<ActionResult> DeleteConnection(Guid id)
    {
        var sid = User.GetIdentity();
        logger.LogInformation("Request to delete connection #{BankConsentId} for {UserId}", id, sid);

        try
        {
            await bankConsentRepository.DeleteConnectionAsync(sid, id);
            
            return new OkResult();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Failed to delete connection #{BankConsentId} for {UserId}", id, sid);
            return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
        }
    }


    public IActionResult Privacy()
    {
        return View();
    }

    [AllowAnonymous]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    private static HomePageViewModel BuildVm(OpenBankingUserDetails openBankingUserDetails, string authUri)
    {
        var vm = new HomePageViewModel
        {
            OpenBankingAuthenticateUri = authUri
        };

        foreach(var connection in openBankingUserDetails.BankConnections)
        {
            var connectionVm = new BankConnectionViewModel
            {
                Id = connection.Id, Provider = connection.Provider ?? "unknown",
                ProviderLogo = connection.ProviderLogoUri, ConsentExpires = connection.Consent.ConsentExpires
            };
            foreach (var account in connection.Accounts)
            {
                connectionVm.Accounts.Add(new AccountHeaderViewModel
                {
                    Id = account.Id, AccountNumber = account.AccountNumber!, Bank = account.ProviderName!,
                    Currency = account.Currency, DisplayName = account.DisplayName, SortCode = account.SortCode!
                });
            }

            vm.BankConnections.Add(connectionVm);
        }
        
        return vm;
    }
}