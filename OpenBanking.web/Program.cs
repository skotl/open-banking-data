using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Identity.Web;
using Microsoft.Identity.Web.UI;
using OpenBanking.Abstract;
using OpenBanking.Models;
using OpenBanking.ScheduledTasks;
using OpenBanking.Storage;
using OpenBanking.TrueLayer;
using OpenBanking.Web.Services;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Get an IsDevelopment flag before we've executed builder.build()
var isDevelopment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

// Serilog
builder.Host.UseSerilog((ctx, lc) => lc
    .WriteTo.Console()
    .ReadFrom.Configuration(ctx.Configuration)
);

try
{
// Set options
    builder.Services.Configure<OpenBankingConfiguration>(
        builder.Configuration.GetSection(OpenBankingConfiguration.Section));

// DI setup
    builder.Services.AddScoped<ICurrencyBulkUpdateService, CurrencyBulkUpdateService>();
    builder.Services.AddScoped<IOpenBankingAuthentication, TrueLayerAuthentication>();
    builder.Services.AddScoped<IOpenBankingIdentity, TrueLayerIdentity>();
    builder.Services.AddScoped<IOpenBankingAccounts, TrueLayerAccounts>();
    builder.Services.AddScoped<IOpenBankingTransactions, TrueLayerTransactions>();
    builder.Services.AddScoped<IBankConnectionService, BankConnectionService>();

    builder.Services.AddHttpClient();

// Add services to the container.
    builder.Services.AddAuthentication(OpenIdConnectDefaults.AuthenticationScheme)
        .AddMicrosoftIdentityWebApp(builder.Configuration.GetSection("AzureAd"));

    builder.Services.AddDatabaseStorage(builder.Configuration.GetConnectionString("DefaultConnection") ?? "not set", isDevelopment);
    builder.Services.AddScheduleTaskSupport();

    builder.Services.AddControllersWithViews(options =>
    {
        var policy = new AuthorizationPolicyBuilder()
            .RequireAuthenticatedUser()
            .Build();
        options.Filters.Add(new AuthorizeFilter(policy));
    });
    builder.Services.AddRazorPages()
        .AddMicrosoftIdentityUI();

    var app = builder.Build();

    app.UseSerilogRequestLogging();

// Configure the HTTP request pipeline.
    if (!app.Environment.IsDevelopment())
    {
        app.UseExceptionHandler("/Home/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
    }

    app.UseHttpsRedirection();
    app.UseStaticFiles();

    app.UseRouting();

    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");
    app.MapRazorPages();

    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "{Message}", "Unhandled exception");
}
finally
{
    Log.Information("Shutdown complete");
    Log.CloseAndFlush();
}