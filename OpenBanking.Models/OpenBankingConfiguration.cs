﻿namespace OpenBanking.Models;

public class OpenBankingConfiguration
{
    public const string Section = "OpenBanking";

    public string Host { get; init; } = string.Empty;
    public string ClientId { get; init; } = string.Empty;
    public string ClientSecret { get; init; } = string.Empty;
    public string RedirectUri { get; init; } = string.Empty;
    public string[] Scopes { get; init; } = Array.Empty<string>();
    public string[] Providers { get; init; } = Array.Empty<string>();
}