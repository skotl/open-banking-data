﻿namespace OpenBanking.Models;

public class OpenBankingTransaction
{
    public enum TransactionTypeEnum
    {
        Credit, Debit    
    }

    public DateTime Updated { get; set; }
    public string AccountId { get; init; } = null!;
    public string ProviderTransactionId { get; set; } = null!;
    public DateTime TimeStamp { get; init; }
    public string Description { get; init; } = null!;
    public TransactionTypeEnum TransactionType { get; init; }
    public string Category { get; init; } = null!;
    public List<string> Classifications { get; init; } = [];
    public OpenBankingTransactionClassification PrimaryClassification { get; init; } = new();
    public OpenBankingTransactionClassification SecondaryClassification { get; init; } = new();
    public string Merchant { get; init; } = null!;
    public OpenBankingMoney Amount { get; init; } = new ();
    public OpenBankingMoney RunningBalance { get; init; } = new ();

    public override string ToString()
    {
        return $"{TimeStamp} {TransactionType} {Amount} {Description}";
    }
}