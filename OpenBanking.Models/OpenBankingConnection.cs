﻿namespace OpenBanking.Models;

public class OpenBankingConnection
{
    public Guid Id { get; init; }
    
    public string UserFullName { get; init; } = null!;

    public string? Provider { get; init; }

    public string? ProviderLogoUri { get; init; }

    public OpenBankingConsent Consent { get; init; } = null!;

    public List<OpenBankingAccount> Accounts { get; init; } = [];
}