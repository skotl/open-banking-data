﻿namespace OpenBanking.Models;

public class OpenBankingAccount
{
    public Guid Id { get; init; }
    
    public DateTime Updated { get; init; }

    public string AccountId { get; init; } = string.Empty;

    public string AccountType { get; init; } = string.Empty;

    public string DisplayName { get; init; } = string.Empty;

    public string Currency { get; init; } = string.Empty;

    public string? AccountNumber { get; init; }

    public string? SortCode { get; init; }

    public string? ProviderName { get; init; }

    public string? ProviderId { get; init; } = string.Empty;

    public string? LogoUri { get; init; }
}