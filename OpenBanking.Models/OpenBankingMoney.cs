﻿namespace OpenBanking.Models;

public class OpenBankingMoney(string currency, decimal? amount)
{
    public string Currency { get; } = currency;
    public decimal? Amount { get; } = amount;

    public OpenBankingMoney() : this("???", null)
    {
    }

    public override string ToString()
    {
        return $"{Currency} {Amount}";
    }
}