﻿namespace OpenBanking.Models;

public class OpenBankingScheduledTask
{
    public enum JobTypes
    {
        GetTransactions,
        PollForTransactions
    }

    public class OptionalData
    {
        public  DateTime? FromDate { get; set; }
        public  DateTime? ToDate { get; set; }
        public string? PollUri { get; set; }
    }

    public Guid Id { get; init; }

    public int TaskType { get; init; }

    public string Description { get; init; } = string.Empty;

    public Guid? BankAccountId { get; init; }

    public DateTime Created { get; init; }

    public DateTime DelayStartUntil { get; init; }

    public DateTime? LastAttemptStartedAt { get; init; }

    public DateTime? LastAttemptCompletedAt { get; init; }

    public bool InProgress { get; init; }

    public int RetryCount { get; init; }

    public int MaxRetries { get; init; }

    public string? Error { get; init; }

    public OptionalData Data { get; } = new ();
}