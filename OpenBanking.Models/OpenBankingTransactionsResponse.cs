﻿namespace OpenBanking.Models;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by JSON serialisation

public class OpenBankingTransactionsResponse
{
    public enum RequestStatusType
    {
        Queued,
        Running,
        Succeeded,
        Failed
    }

    public RequestStatusType RequestStatus { get; set; }

    public List<OpenBankingTransaction> Transactions { get; set; } = [];
}