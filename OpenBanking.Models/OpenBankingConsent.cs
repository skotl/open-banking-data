﻿namespace OpenBanking.Models;

public class OpenBankingConsent
{
    public Guid Id { get; set; }

    public string AccessToken { get; set; } = null!;

    public string RefreshToken { get; set; } = null!;

    /// <summary>
    /// The date and time that consent was granted
    /// </summary>
    public DateTime ConsentGranted { get; set; }

    /// <summary>
    /// The date and time that consent will expire (note that <see cref="RefreshTokenExpires"/> will
    /// cause consent to be revoked if the refresh token is not used by then)
    /// </summary>
    public DateTime ConsentExpires => ConsentGranted.AddDays(90);

    /// <summary>
    /// The date and time that the refresh token will expire - tokens must be refreshed every 30 days otherwise
    /// consent will be invalidated
    /// <para>https://docs.truelayer.com/docs/overview-of-integration-steps#step-3-configuring-the-backend-to-use-data-api</para>
    /// </summary>
    public DateTime RefreshTokenExpires { get; set; }

    public DateTime TokenExpiryUtc { get; set; }
}