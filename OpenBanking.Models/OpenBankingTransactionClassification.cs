﻿namespace OpenBanking.Models;

public class OpenBankingTransactionClassification
{
    public Guid? Id { get; set; }
    public string? Name { get; set; }
}