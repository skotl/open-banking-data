﻿namespace OpenBanking.Models;

public class OpenBankingScheduledTaskMessage
{
    public Guid ScheduledTaskMessageId { get; set; }

    public int TaskType { get; set; }

    public string Description { get; set; } = string.Empty;

    public Guid? BankAccountId { get; set; }

    public DateTime Created { get; set; }

    public string Message { get; set; } = string.Empty;
}