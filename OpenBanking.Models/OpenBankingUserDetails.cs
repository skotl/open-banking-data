﻿namespace OpenBanking.Models;

public class OpenBankingUserDetails
{
    public string UserId { get; init; } = string.Empty;

    public List<OpenBankingConnection> BankConnections { get; init; } = [];
}