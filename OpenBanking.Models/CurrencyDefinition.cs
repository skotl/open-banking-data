﻿namespace OpenBanking.Models;

public class CurrencyDefinition
{
    public string Code { get; init; } = string.Empty;

    public string Region { get; init; } = string.Empty;

    public string Name { get; init; } = string.Empty;

    public byte[] Flag { get; init; } = null!;
    
    public string Symbol  { get; init; } = null!;

    public override string ToString()
    {
        return Code;
    }
}