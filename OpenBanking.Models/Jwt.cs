﻿using System.Diagnostics.CodeAnalysis;

namespace OpenBanking.Models;

[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Json serialized")]
public class Jwt
{
    public string AccessToken { get; init; } = string.Empty;
    public DateTime ExpiryUtc { get; init; }
    public string TokenType { get; init; } = string.Empty;
    public string RefreshToken { get; init; } = string.Empty;
}