﻿using OpenBanking.Models;

namespace OpenBanking.ScheduledTasks.Orchestration;

public interface IJobDispatcher
{
    Task DispatchAsync(OpenBankingScheduledTask task);
}