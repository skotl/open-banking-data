﻿using Microsoft.Extensions.Logging;
using OpenBanking.Models;
using OpenBanking.ScheduledTasks.JobExecution;

// ReSharper disable SuggestBaseTypeForParameterInConstructor

namespace OpenBanking.ScheduledTasks.Orchestration;

internal class JobDispatcher(
    ILogger<JobDispatcher> logger,
    IPollAccountTransactionsJob pollAccountTransactionsJob,
    IRequestAccountTransactionsJob requestAccountTransactionsJob)
    : IJobDispatcher
{
    public async Task DispatchAsync(OpenBankingScheduledTask task)
    {
        logger.LogInformation("Dispatching job #{JobId}: {Desc}", task.Id, task.Description);

        switch ((OpenBankingScheduledTask.JobTypes)task.TaskType)
        {
            case OpenBankingScheduledTask.JobTypes.GetTransactions:
                await requestAccountTransactionsJob.ExecuteAsync(task);
                break;
            case OpenBankingScheduledTask.JobTypes.PollForTransactions:
                await pollAccountTransactionsJob.ExecuteAsync(task);
                break;

            default: throw new InvalidOperationException($"Don't know how to dispatch job type {task.TaskType}");
        }
    }
}