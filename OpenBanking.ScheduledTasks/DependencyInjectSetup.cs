﻿using Microsoft.Extensions.DependencyInjection;
using OpenBanking.ScheduledTasks.JobExecution;
using OpenBanking.ScheduledTasks.Orchestration;
using OpenBanking.ScheduledTasks.PublicServices;

namespace OpenBanking.ScheduledTasks;

public static class DependencyInjectSetup
{
    // ReSharper disable once UnusedMethodReturnValue.Global
    // Reason: Recommended practise, so caller can call ...AddScheduleTaskSupport().AddOther().UseOther();
    public static IServiceCollection AddScheduleTaskSupport(this IServiceCollection services)
    {
        // Services DI
        services.AddScoped<IJobDispatcher, JobDispatcher>();
        services.AddScoped<IGenerateHistoricTransactionsRequestService, GenerateHistoricTransactionsRequestService>();
        services.AddTransient<IGenerateHistoricTransactionsRequestService, GenerateHistoricTransactionsRequestService>();
        services.AddTransient<IGenerateAccountTransactionsRequestService, GenerateAccountTransactionsRequestService>();
        services.AddTransient<IPollAccountTransactionsJob, PollAccountTransactionsJob>();
        services.AddTransient<IRequestAccountTransactionsJob, RequestAccountTransactionsJob>();
        services.AddTransient
            <IGenerateAccountTransactionsPollRequestService, GenerateAccountTransactionsPollRequestService>();
        services.AddTransient<IDestroyAllScheduledJobsService, DestroyAllScheduledJobsService>();

        return services;
    }
}