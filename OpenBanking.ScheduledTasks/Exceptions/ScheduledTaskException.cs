﻿namespace OpenBanking.ScheduledTasks.Exceptions;

public class ScheduledTaskException(string msg, Exception ex) : Exception(msg, ex);
