﻿namespace OpenBanking.ScheduledTasks.PublicServices;

public interface IGenerateAccountTransactionsPollRequestService
{
    Task GenerateAsync(Guid accountId, string pollUri, string originalDescription);
}