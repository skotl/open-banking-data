﻿using Microsoft.Extensions.Logging;
using OpenBanking.Storage.Repositories;

namespace OpenBanking.ScheduledTasks.PublicServices;

internal class DestroyAllScheduledJobsService : IDestroyAllScheduledJobsService
{
    private readonly ILogger<DestroyAllScheduledJobsService> _logger;
    private readonly IScheduledTaskRepository _scheduledTaskRepository;

    public DestroyAllScheduledJobsService(ILogger<DestroyAllScheduledJobsService> logger, IScheduledTaskRepository scheduledTaskRepository)
    {
        _logger = logger;
        _scheduledTaskRepository = scheduledTaskRepository;
    }
    
    public async Task ExecuteAsync()
    {
        _logger.LogCritical("All scheduled jobs and triggers being deleted!");

        await _scheduledTaskRepository.RemoveAllAsync();
    }
}