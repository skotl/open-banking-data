﻿using Microsoft.Extensions.Logging;
using OpenBanking.Storage.Repositories;

namespace OpenBanking.ScheduledTasks.PublicServices;

internal class GenerateAccountTransactionsPollRequestService(
    ILogger<GenerateAccountTransactionsPollRequestService> logger,
    IScheduledTaskRepository scheduledTaskRepository)
    : IGenerateAccountTransactionsPollRequestService
{
    public async Task GenerateAsync(Guid accountId, string pollUri, string originalDescription)
    {
        logger.LogDebug("Generating transactions poll request for account#{AccountId} at {Uri}", accountId, pollUri);

        var description = BuildDescription(pollUri, originalDescription);

        await scheduledTaskRepository.AddPollForTransactionsRequestAsync(accountId, description, pollUri);
    }

    private static string BuildDescription(string pollUri, string originalDescription)
    {
        return $"Poll for {originalDescription}, from {pollUri}";
    }
}