﻿namespace OpenBanking.ScheduledTasks.PublicServices;

public interface IGenerateHistoricTransactionsRequestService
{
    Task GenerateRequestsAsync(Guid accountId);
}