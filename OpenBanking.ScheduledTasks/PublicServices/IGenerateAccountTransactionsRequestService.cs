﻿namespace OpenBanking.ScheduledTasks.PublicServices;

public interface IGenerateAccountTransactionsRequestService
{
    Task GenerateAsync(Guid accountId, DateTime fromDate, DateTime toDate);
}