﻿using Microsoft.Extensions.Logging;

namespace OpenBanking.ScheduledTasks.PublicServices;

internal class GenerateHistoricTransactionsRequestService(
    ILogger<GenerateHistoricTransactionsRequestService> logger,
    IGenerateAccountTransactionsRequestService generateAccountTransactionsRequestService)
    : IGenerateHistoricTransactionsRequestService
{
    private const int HistoricMaxMonths = 24;
    private const int BatchSizeMonths = 3;

    public async Task GenerateRequestsAsync(Guid accountId)
    {
        logger.LogInformation("Generating historic transaction requests for account#{AccountId}", accountId);

        var absoluteHardStop = DateTime.UtcNow.AddHours(-1);    // HTTP 400 if we are too close to right-now

        var startDate = DateTime.UtcNow.AddMonths(-HistoricMaxMonths);
        while(startDate <= absoluteHardStop)
        {
            var endDate = startDate.AddMonths(BatchSizeMonths);
            if (endDate > absoluteHardStop)
                endDate = absoluteHardStop;

            logger.LogInformation("Request for #{AccountID} from {Start} to {End}", accountId, startDate, endDate);

            await generateAccountTransactionsRequestService.GenerateAsync(accountId, startDate, endDate);

            startDate = endDate.AddMinutes(1);
        }
    }
}