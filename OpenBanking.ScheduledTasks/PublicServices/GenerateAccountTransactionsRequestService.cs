﻿using Microsoft.Extensions.Logging;
using OpenBanking.Storage.Repositories;

namespace OpenBanking.ScheduledTasks.PublicServices;

internal class GenerateAccountTransactionsRequestService(
    ILogger<GenerateAccountTransactionsRequestService> logger,
    IScheduledTaskRepository scheduledTaskRepository)
    : IGenerateAccountTransactionsRequestService
{
    public async Task GenerateAsync(Guid accountId, DateTime fromDate, DateTime toDate)
    {
        logger.LogDebug("Generating request for transactions for account#{AccountId} from {Start}-{End}",
            accountId, fromDate, toDate);

        var description = BuildDescription(accountId, fromDate, toDate);

        await scheduledTaskRepository.AddTransactionsRequestJobAsync(accountId, description, fromDate, toDate);
    }

    private static string BuildDescription(Guid accountId, DateTime fromDate, DateTime toDate)
    {
        return $"Account transactions for {accountId}, from {fromDate} to {toDate}";
    }
}