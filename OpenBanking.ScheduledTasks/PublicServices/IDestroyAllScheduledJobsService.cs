﻿namespace OpenBanking.ScheduledTasks.PublicServices;

public interface IDestroyAllScheduledJobsService
{
    Task ExecuteAsync();
}