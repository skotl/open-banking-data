﻿using OpenBanking.Models;

namespace OpenBanking.ScheduledTasks.JobExecution;

public interface IJobExecution
{
    Task ExecuteAsync(OpenBankingScheduledTask task);
}