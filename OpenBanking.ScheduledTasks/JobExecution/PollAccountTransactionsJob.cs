﻿using Microsoft.Extensions.Logging;
using OpenBanking.Abstract;
using OpenBanking.Abstract.Exceptions;
using OpenBanking.Models;
using OpenBanking.ScheduledTasks.Exceptions;
using OpenBanking.Storage.Repositories;

namespace OpenBanking.ScheduledTasks.JobExecution;

// ReSharper disable once ClassNeverInstantiated.Global
// Reason: created by JobBuilder.Create()
internal class PollAccountTransactionsJob(
    ILogger<PollAccountTransactionsJob> logger,
    IBankConsentRepository bankConsentRepository,
    IOpenBankingTransactions openBankingTransactions,
    IBankAccountRepository bankAccountRepository,
    IBankTransactionRepository bankTransactionRepository,
    IScheduledTaskRepository scheduledTaskRepository)
    : IPollAccountTransactionsJob
{
    public async Task ExecuteAsync(OpenBankingScheduledTask task)
    {
        try
        {
            if (!task.BankAccountId.HasValue)
                throw new InvalidDataException($"Account ID is missing on scheduled task #{task.Id}");
            if (string.IsNullOrWhiteSpace(task.Data.PollUri))
                throw new InvalidDataException($"Poll URI is missing on scheduled task #{task.Id}");

            logger.LogInformation("Executing #{TaskId}, {Desc}", task.Id, task.Description);

            var accountId = task.BankAccountId.Value;
            var accessToken = await bankConsentRepository.GetAccessTokenForBankAccount(accountId);
            var account = await bankAccountRepository.GetAsync(accountId);

            var result =
                await openBankingTransactions.PollForTransactionResponseAsync(account.AccountId, task.Data.PollUri,
                    accessToken);

            logger.LogInformation("Task #{TaskId} completed with status = {Result}",
                task.Id, result.RequestStatus.ToString());

            switch (result.RequestStatus)
            {
                case OpenBankingTransactionsResponse.RequestStatusType.Queued:
                case OpenBankingTransactionsResponse.RequestStatusType.Running:
                    await scheduledTaskRepository.DeferJob(task.Id, 60);
                    break;
                case OpenBankingTransactionsResponse.RequestStatusType.Succeeded:
                    await bankTransactionRepository.InsertOrUpdateAsync(accountId, account.AccountId, result.Transactions);
                    break;
                case OpenBankingTransactionsResponse.RequestStatusType.Failed:
                default:
                    throw new OpenBankingPollTransactionsException(
                        $"Request failed for job #{task.Id}, {task.Description}");
            }
        }
        catch (Exception ex)
        {
            var jobException = new ScheduledTaskException(ex.Message, ex);

            logger.LogError(jobException, "{Error}", ex.Message);
            throw jobException;
        }
    }
}