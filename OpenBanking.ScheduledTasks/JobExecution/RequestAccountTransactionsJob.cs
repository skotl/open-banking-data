﻿using Microsoft.Extensions.Logging;
using OpenBanking.Abstract;
using OpenBanking.Models;
using OpenBanking.ScheduledTasks.Exceptions;
using OpenBanking.ScheduledTasks.PublicServices;
using OpenBanking.Storage.Repositories;

namespace OpenBanking.ScheduledTasks.JobExecution;

internal class RequestAccountTransactionsJob(
    ILogger<RequestAccountTransactionsJob> logger,
    IBankConsentRepository bankConsentRepository,
    IOpenBankingTransactions openBankingTransactions,
    IBankAccountRepository bankAccountRepository,
    IGenerateAccountTransactionsPollRequestService generateAccountTransactionsPollRequestService)
    : IRequestAccountTransactionsJob
{
    public async Task ExecuteAsync(OpenBankingScheduledTask task)
    {
        if (!task.BankAccountId.HasValue)
            throw new InvalidDataException($"Account ID is missing on scheduled task #{task.Id}");
        if (!task.Data.FromDate.HasValue)
            throw new InvalidDataException($"Account ID is missing on scheduled task #{task.Data.FromDate}");
        if (!task.Data.ToDate.HasValue)
            throw new InvalidDataException($"Account ID is missing on scheduled task #{task.Data.ToDate}");

        var accountId = task.BankAccountId.Value;

        try
        {
            var accessToken = await bankConsentRepository.GetAccessTokenForBankAccount(accountId);
            var account = await bankAccountRepository.GetAsync(accountId);
            var uri = await openBankingTransactions.RequestTransactionListAsync(account.AccountId,
                task.Data.FromDate.Value, task.Data.ToDate.Value, accessToken);

            logger.LogInformation("Executing #{TaskId}, {Desc}", task.Id, task.Description);

            // Response is a URL to poll, so schedule a new job for this
            await generateAccountTransactionsPollRequestService.GenerateAsync(accountId, uri, task.Description);
        }
        catch (Exception ex)
        {
            var jobException = new ScheduledTaskException(ex.Message, ex);
            
            logger.LogError(jobException, "{Error}", ex.Message);
            throw jobException;
        }
    }
}