﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingInvalidCurrencyException : Exception
{
    public OpenBankingInvalidCurrencyException(string msg) :base(msg)
    {}
}