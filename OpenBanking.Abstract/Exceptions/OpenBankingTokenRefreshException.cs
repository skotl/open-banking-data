﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingTokenRefreshException(string msg) : Exception(msg);