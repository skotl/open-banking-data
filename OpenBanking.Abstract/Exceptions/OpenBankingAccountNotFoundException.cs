﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingAccountNotFoundException : Exception
{
    public OpenBankingAccountNotFoundException(string msg) : base(msg)
    {}
}