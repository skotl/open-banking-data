﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingPollTransactionsException : Exception
{
    public OpenBankingPollTransactionsException(string msg) : base(msg)
    {}
}