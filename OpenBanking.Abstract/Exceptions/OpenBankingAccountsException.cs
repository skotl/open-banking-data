﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingAccountsException(string msg) : Exception(msg);