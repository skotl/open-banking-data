﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingAccountIdMismatchException : Exception
{
    public OpenBankingAccountIdMismatchException(string msg) : base(msg)
    {}
}