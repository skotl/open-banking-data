﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingConsentHasExpiredException : Exception
{
    public OpenBankingConsentHasExpiredException(string msg) : base(msg)
    {}
}