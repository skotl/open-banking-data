﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingConsentNotFoundException : Exception
{
    public OpenBankingConsentNotFoundException(string msg)
        : base(msg)
    {}
}