﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingAuthenticationException(string msg) : Exception(msg);