﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingScheduledTaskException : Exception
{
    public OpenBankingScheduledTaskException(string msg)
        :base(msg)
    {}
}