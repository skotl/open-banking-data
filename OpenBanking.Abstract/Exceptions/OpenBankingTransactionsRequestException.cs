﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingTransactionsRequestException : Exception
{
    public OpenBankingTransactionsRequestException(string msg) : base(msg)
    {}
}