﻿namespace OpenBanking.Abstract.Exceptions;

public class OpenBankingIdentityException(string msg) : Exception(msg);