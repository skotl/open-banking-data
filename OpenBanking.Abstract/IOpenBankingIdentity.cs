﻿
namespace OpenBanking.Abstract;

public interface IOpenBankingIdentity
{
    Task<string> GetIdentityAsync(string accessToken, string userId);
}