﻿using OpenBanking.Models;

namespace OpenBanking.Abstract;

public interface IOpenBankingAccounts
{
    Task<List<OpenBankingAccount>> GetAccountListAsync(string accessToken, string userId);
}