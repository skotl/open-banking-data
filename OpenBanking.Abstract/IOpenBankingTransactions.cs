﻿using OpenBanking.Models;

namespace OpenBanking.Abstract;

public interface IOpenBankingTransactions
{
    Task<string> RequestTransactionListAsync(string accountId, DateTime fromDate, DateTime toDate, string accessToken);
    Task<OpenBankingTransactionsResponse> PollForTransactionResponseAsync(string accountId, string uri, string accessToken);
}