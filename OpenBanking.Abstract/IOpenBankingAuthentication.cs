﻿using OpenBanking.Models;

namespace OpenBanking.Abstract;

public interface IOpenBankingAuthentication
{
    string BuildAuthenticationUri(string host, string clientId, string redirectUri, IEnumerable<string> scopes, IEnumerable<string> providers);
    Task<Jwt> ConvertCodeAsync(string userId, string clientId, string clientSecret, string redirectUrl, string code);
    Task<Jwt> RefreshAccessTokenAsync(string userId, string clientId, string clientSecret, string refreshToken);
}