﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OpenBanking.Storage.Repositories;

namespace OpenBanking.Storage;

public static class DependencyInjectionSetup
{
    // ReSharper disable once UnusedMethodReturnValue.Global
    // Reason: Recommended practise, so caller can call ...AddDatabaseStorage().AddOther().UseOther();
    public static IServiceCollection AddDatabaseStorage(this IServiceCollection services, string connectionString,
        bool enableDebugPages)
    {
        services.AddDbContextFactory<OpenBankingContext>(options =>
            options.UseSqlServer(connectionString));

        if (enableDebugPages)
            services.AddDatabaseDeveloperPageExceptionFilter();

        services.AddScoped<IBankAccountRepository, BankAccountRepository>();
        services.AddScoped<IBankConsentRepository, BankConsentRepository>();
        services.AddScoped<IBankingProviderRepository, BankingProviderRepository>();
        services.AddScoped<IBankTransactionRepository, BankTransactionRepository>();
        services.AddScoped<IBankTransactionClassificationRepository, BankTransactionClassificationRepository>();
        services.AddScoped<IScheduledTaskRepository, ScheduledTaskRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<ICurrencyRepository, CurrencyRepository>();
        
        return services;
    }
}