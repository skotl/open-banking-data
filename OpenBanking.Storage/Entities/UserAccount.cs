﻿using System.ComponentModel.DataAnnotations;

namespace OpenBanking.Storage.Entities;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework
internal class UserAccount
{
    [MaxLength(64)]
    public string UserAccountId { get; set; } = string.Empty;
    
    [Timestamp]
    public byte[] Timestamp { get; set; } = [];
    
    public List<BankConsent> BankConsents { get; set; } = [];
}