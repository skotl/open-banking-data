﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace OpenBanking.Storage.Entities;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework
[Index(nameof(Created), IsUnique = false)]
[Index(nameof(DelayStartUntil), IsUnique = false)]
internal class ScheduledTask
{
    [Timestamp]
    public byte[] Timestamp { get; set; } = [];

    public Guid ScheduledTaskId { get; set; }

    public int TaskType { get; set; }

    [MaxLength(2048)]
    public string Description { get; set; } = string.Empty;

    public Guid? BankAccountId { get; set; }

    public DateTime Created { get; set; }

    public DateTime DelayStartUntil { get; set; }

    public DateTime? DataFrom { get; set; }

    public DateTime? DataTo { get; set; }

    [MaxLength(1024)]
    public string? DataPollUri { get; set; }

    public DateTime? LastAttemptStartedAt { get; set; }

    public DateTime? LastAttemptCompletedAt { get; set; }

    public bool InProgress { get; set; }

    public int RetryCount { get; set; }

    public int MaxRetries { get; set; }

    [MaxLength(2048)]
    public string? Error { get; set; }
}