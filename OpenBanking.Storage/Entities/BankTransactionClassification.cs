﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace OpenBanking.Storage.Entities;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework
[Index(nameof(Name), IsUnique = true)]
internal class BankTransactionClassification
{
    [Timestamp]
    public byte[] Timestamp { get; set; } = null!;

    public Guid Id { get; set; }

    [MaxLength(128)]
    public string Name { get; set; } = string.Empty;
}