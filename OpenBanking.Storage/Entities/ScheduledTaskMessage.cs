﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace OpenBanking.Storage.Entities;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework
[Index(nameof(Created), IsUnique = false)]
[Index(nameof(BankAccountId), nameof(Created), IsUnique = false)]
[Index(nameof(OriginalTaskId), nameof(Created), IsUnique = false)]
internal class ScheduledTaskMessage
{
    [Timestamp]
    public byte[] Timestamp { get; set; } = [];

    public Guid ScheduledTaskMessageId { get; set; }

    public Guid OriginalTaskId { get; set; }

    public int TaskType { get; set; }

    [MaxLength(2048)]
    public string Description { get; set; } = string.Empty;

    public Guid? BankAccountId { get; set; }

    public DateTime Created { get; set; }

    [MaxLength(4096)]
    public string Message { get; set; } = string.Empty;
}