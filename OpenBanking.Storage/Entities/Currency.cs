﻿using System.ComponentModel.DataAnnotations;

namespace OpenBanking.Storage.Entities;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework
internal class Currency
{
    [Timestamp]
    public byte[] Timestamp { get; set; } = null!;

    [Key]
    [MaxLength(3)]
    public string Code { get; set; } = string.Empty;

    [MaxLength(64)]
    public string Region { get; set; } = string.Empty;

    [MaxLength(64)]
    public string Name { get; set; } = string.Empty;

    public byte[] Flag { get; set; } = null!;
    
    [MaxLength(10)]
    public string Symbol  { get; set; } = null!;

    public override string ToString()
    {
        return Code;
    }
}