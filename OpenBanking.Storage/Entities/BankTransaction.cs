﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace OpenBanking.Storage.Entities;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework

[Index(nameof(AccountId), nameof(ProviderTransactionId), IsUnique = true)]
[Index(nameof(AccountId), nameof(TransactionTime))]
[Index(nameof(PrimaryClassificationId), nameof(Id), IsUnique = true)]
[Index(nameof(SecondaryClassificationId), nameof(Id), IsUnique = true)]
[Index(nameof(PrimaryClassificationId), nameof(SecondaryClassificationId), nameof(Id), IsUnique = true)]
internal class BankTransaction
{
    public enum TransactionTypeEnum
    {
        Credit, Debit    
    }
    
    [Timestamp]
    public byte[] Timestamp { get; set; } = null!;
    
    public Guid Id { get; set; }

    public BankAccount Account { get; set; } = null!;
    public Guid AccountId { get; set; }

    [MaxLength(256)]
    public string ProviderTransactionId { get; set; } = null!;
    
    public DateTime Updated { get; set; } = DateTime.UtcNow;
    
    public DateTime TransactionTime { get; set; }
    
    [MaxLength(1024)]
    public string? Description { get; set; }
    public TransactionTypeEnum TransactionType { get; set; }
    
    [MaxLength(256)]
    public string? Category { get; set; }
    
    [MaxLength(256)]
    public string? Merchant { get; set; }
    
    public Currency Currency { get; set; } = null!;
    [MaxLength(3)]
    public string CurrencyId { get; set; } = null!;
    public decimal Amount { get; set; }
    
    public Currency? RunningBalanceCurrency { get; set; }
    [MaxLength(3)]
    public string? RunningBalanceCurrencyId { get; set; }
    public decimal? RunningBalanceAmount { get; set; }

    public Guid? PrimaryClassificationId { get; set; }
    public BankTransactionClassification? PrimaryClassification { get; set; }

    public Guid? SecondaryClassificationId { get; set; }
    public BankTransactionClassification? SecondaryClassification { get; set; }
}