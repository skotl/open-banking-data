﻿using System.ComponentModel.DataAnnotations;

namespace OpenBanking.Storage.Entities;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework

internal class BankConsent
{
    [Timestamp]
    public byte[] Timestamp { get; set; } = [];

    public Guid BankConsentId { get; set; }

    public UserAccount UserAccount { get; set; } = null!;

    [MaxLength(64)]
    public string UserAccountId { get; set; } = string.Empty;

    public Guid BankingProviderId { get; set; }

    public BankingProvider BankingProvider { get; set; }  = null!;

    [MaxLength(256)]
    public string UserFullName { get; set; } = string.Empty;
    
    [MaxLength(8192)]
    public string AccessToken { get; set; } = string.Empty;

    [MaxLength(8192)]
    public string RefreshToken { get; set; } = string.Empty;
    
    public DateTime ConsentGranted { get; set; }
    
    public DateTime ConsentExpires { get; set; }
    
    public DateTime RefreshTokenExpires { get; set; }

    public DateTime TokenExpiryUtc { get; set; }

    public List<BankAccount> BankAccounts { get; set; } = [];
}