﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace OpenBanking.Storage.Entities;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework

[Index( nameof(BankConsentId), nameof(ProviderAccountId), IsUnique = true)]
internal class BankAccount
{
    [Timestamp]
    public byte[] Timestamp { get; set; } = [];

    public Guid BankAccountId { get; set; }

    public BankConsent BankConsent { get; set; } = null!;

    public Guid BankConsentId { get; set; }

    public DateTime Updated { get; set; }

    [MaxLength(256)]
    public string ProviderAccountId { get; set; } = string.Empty;
    
    [MaxLength(64)] 
    public string AccountType { get; set; } = string.Empty;
    
    [MaxLength(256)]
    public string DisplayName { get; set; } = string.Empty;

    [MaxLength(10)]
    public string Currency { get; set; } = string.Empty;

    [MaxLength(8)]
    public string? SortCode { get; set; }

    [MaxLength(32)]
    public string? AccountNumber { get; set; }
}