﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace OpenBanking.Storage.Entities;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework

[Index(nameof(Name), IsUnique = false)]
[Index(nameof(OpenBankingProviderId), IsUnique = true)]
internal class BankingProvider
{
    [Timestamp]
    public byte[] Timestamp { get; set; } = null!;

    public Guid BankingProviderId { get; set; }

    [MaxLength(128)]
    public string Name { get; set; } = null!;
    
    [MaxLength(128)]
    public string OpenBankingProviderId { get; set; } = null!;
    
    [MaxLength(512)]
    public string? LogoUri { get; set; }
    
    public List<BankConsent> BankConsents { get; set; } = null!;
}