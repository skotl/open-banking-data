﻿using OpenBanking.Models;

namespace OpenBanking.Storage.Repositories;

public interface ICurrencyRepository
{
    Task InsertOrUpdateAsync(CurrencyDefinition currency);
    Task<List<CurrencyDefinition>> GetAllAsync(bool includeFlags = false);
}