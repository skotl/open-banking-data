﻿using OpenBanking.Models;

namespace OpenBanking.Storage.Repositories;

public interface IBankAccountRepository
{
    Task<List<Guid>> AddOrUpdateAsync(Guid consentId, List<OpenBankingAccount> accounts);
    Task<OpenBankingAccount> GetAsync(Guid accountId);
}