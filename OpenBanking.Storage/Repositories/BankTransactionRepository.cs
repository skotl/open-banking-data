﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBanking.Abstract.Exceptions;
using OpenBanking.Models;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.Repositories;

internal class BankTransactionRepository : IBankTransactionRepository
{
    private readonly ILogger<BankTransactionRepository> _logger;
    private readonly IDbContextFactory<OpenBankingContext> _contextFactory;
    private readonly ICurrencyRepository _currencyRepository;
    private readonly IBankTransactionClassificationRepository _bankTransactionClassificationRepository;

    public BankTransactionRepository(
        ILogger<BankTransactionRepository> logger,
        IDbContextFactory<OpenBankingContext> contextFactory,
        ICurrencyRepository currencyRepository,
        IBankTransactionClassificationRepository bankTransactionClassificationRepository)
    {
        _logger = logger;
        _contextFactory = contextFactory;
        _currencyRepository = currencyRepository;
        _bankTransactionClassificationRepository = bankTransactionClassificationRepository;

        _logger.LogDebug("New {Repo} created", nameof(BankTransactionRepository));
    }


    public async Task InsertOrUpdateAsync(Guid accountId, string providerAccountId,
        List<OpenBankingTransaction> transactions)
    {
        ArgumentException.ThrowIfNullOrWhiteSpace(providerAccountId);
        ArgumentNullException.ThrowIfNull(transactions);

        _logger.LogInformation("Inserting or updating {Count} transactions for account#{AccountId}", transactions.Count,
            providerAccountId);

        await using var context = await _contextFactory.CreateDbContextAsync();

        ValidateAccountIds(transactions, providerAccountId);
        await ValidateCurrencyIds(transactions);

        foreach (var model in transactions)
        {
            await UpdateClassifications(model);
            await InsertOrUpdateAsync(accountId, model, context);
        }

        await context.SaveChangesAsync();
    }

    private async Task UpdateClassifications(OpenBankingTransaction model)
    {
        if (model.Classifications.Count >= 1)
            await UpdateClassification(model.PrimaryClassification, model.Classifications[0]);

        if (model.Classifications.Count >= 2)
            await UpdateClassification(model.SecondaryClassification, model.Classifications[1]);
    }

    private async Task UpdateClassification(OpenBankingTransactionClassification classification, string name)
    {
        classification.Name = name;
        classification.Id = await _bankTransactionClassificationRepository.FindOrCreateAsync(name);
    }

    private static void ValidateAccountIds(IEnumerable<OpenBankingTransaction> transactions, string accountId)
    {
        var nonMatching = transactions.Count(t => t.AccountId != accountId);

        if (nonMatching > 0)
            throw new OpenBankingAccountIdMismatchException(
                $"{nonMatching} transaction(s) do not have the account ID={accountId}");
    }

    private async Task ValidateCurrencyIds(IList<OpenBankingTransaction> transactions)
    {
        var currencies = (await _currencyRepository.GetAllAsync(includeFlags: false))
            .Select(c => c.Code);


        var failing = transactions.Where(t => !currencies.Contains(t.Amount.Currency))
            .Select(t => t.Amount.Currency)
            .Union(
                transactions.Where(t => !string.IsNullOrWhiteSpace(t.RunningBalance.Currency)
                                        && !currencies.Contains(t.RunningBalance.Currency))
                    .Select(t => t.RunningBalance.Currency)
            ).ToList();

        if (failing.Count > 0)
            throw new OpenBankingInvalidCurrencyException(string.Join(", ", failing));
    }

    private async Task InsertOrUpdateAsync(Guid accountId, OpenBankingTransaction model, OpenBankingContext context)
    {
        _logger.LogDebug("Saving transaction#{TransactionId} for account#{AccountId}",
            model.ProviderTransactionId, model.AccountId);

        var dto = await context.BankTransactions
            .Include(t => t.Account)
            .FirstOrDefaultAsync(t =>
                t.Account.ProviderAccountId == model.AccountId &&
                t.ProviderTransactionId == model.ProviderTransactionId);

        var newRecord = dto == default;
        if (newRecord)
            dto = new BankTransaction();

        dto!.AccountId = accountId;
        dto.Amount = model.Amount.Amount ?? decimal.Zero;
        dto.Category = model.Category;
        dto.CurrencyId = model.Amount.Currency;
        dto.Description = model.Description;
        dto.Merchant = model.Merchant;
        dto.Updated = DateTime.UtcNow;
        dto.TransactionTime = model.TimeStamp;
        dto.TransactionType = model.TransactionType == OpenBankingTransaction.TransactionTypeEnum.Credit
            ? BankTransaction.TransactionTypeEnum.Credit
            : BankTransaction.TransactionTypeEnum.Debit;
        dto.ProviderTransactionId = model.ProviderTransactionId;
        dto.RunningBalanceAmount = model.RunningBalance.Amount;
        dto.RunningBalanceCurrencyId = model.RunningBalance.Currency;
        dto.PrimaryClassificationId = model.PrimaryClassification.Id;
        dto.SecondaryClassificationId = model.SecondaryClassification.Id;

        if (newRecord)
            await context.BankTransactions.AddAsync(dto);
        else
            context.Update(dto);
    }
}