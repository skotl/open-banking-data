﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBanking.Models;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.Repositories;

internal class CurrencyRepository : ICurrencyRepository
{
    private readonly ILogger<CurrencyRepository> _logger;
    private readonly IDbContextFactory<OpenBankingContext> _contextFactory;

    public CurrencyRepository(ILogger<CurrencyRepository> logger, IDbContextFactory<OpenBankingContext> contextFactory)
    {
        _logger = logger;
        _contextFactory = contextFactory;

        _logger.LogDebug("New {Repo} created", nameof(CurrencyRepository));
    }
    
    public async Task InsertOrUpdateAsync(CurrencyDefinition currency)
    {
        _logger.LogDebug("Insert/Update currency {Currency}", currency);

        await using var context = await _contextFactory.CreateDbContextAsync();

        var dto = await context.Currencies.FirstOrDefaultAsync(c => c.Code == currency.Code);
        if (dto == default)
        {
            _logger.LogDebug("Inserting currency {Currency}", currency);
            dto = new Currency();
            await context.Currencies.AddAsync(dto);
        }

        dto.Code = currency.Code;
        dto.Flag = currency.Flag;
        dto.Name = currency.Name;
        dto.Region = currency.Region;
        dto.Symbol = currency.Symbol;

        await context.SaveChangesAsync();
    }

    public async Task<List<CurrencyDefinition>> GetAllAsync(bool includeFlags = false)
    {
        _logger.LogDebug("Getting all currencies, includeFlags={IncludeFlags}", includeFlags);

        await using var context = await _contextFactory.CreateDbContextAsync();

        var currencies = context.Currencies.AsNoTracking();
        
        if (!includeFlags)
            currencies = currencies.Select(c => new Currency
            {
                Code = c.Code,
                Region = c.Region,
                Name = c.Name,
                Symbol = c.Symbol
            });

        return (await currencies.ToListAsync()).
            Select(c => new CurrencyDefinition
            {
                Code = c.Code,
                Region = c.Region,
                Name = c.Name,
                Flag = c.Flag,
                Symbol = c.Symbol
            })
            .ToList();
    }
}