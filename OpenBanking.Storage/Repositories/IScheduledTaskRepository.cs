﻿using OpenBanking.Models;

namespace OpenBanking.Storage.Repositories;

public interface IScheduledTaskRepository
{
    Task<Guid> AddTransactionsRequestJobAsync(Guid? accountId, string description, DateTime from, DateTime to);
    Task<Guid> AddPollForTransactionsRequestAsync(Guid? accountId, string description, string pollUri);
    Task DeferJob(Guid id, int deferSecondsMultiple);
    Task MarkJobInProgressAsync(Guid id);
    Task ClearJobInProgressAsync(Guid id);
    Task<List<OpenBankingScheduledTask>> GetOutstandingJobsAsync();
    Task RemoveErroredJob(Guid jobId, string message);
    Task RemoveIfNotDeferred(Guid jobId, string message);
    Task RemoveAllAsync();
}