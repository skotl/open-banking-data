﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBanking.Abstract.Exceptions;
using OpenBanking.Models;
using OpenBanking.Storage.DtoTranslation;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.Repositories;

internal class BankAccountRepository : IBankAccountRepository
{
    private readonly ILogger<BankAccountRepository> _logger;
    private readonly IDbContextFactory<OpenBankingContext> _contextFactory;

    public BankAccountRepository(ILogger<BankAccountRepository> logger, IDbContextFactory<OpenBankingContext> contextFactory)
    {
        _logger = logger;
        _contextFactory = contextFactory;

        _logger.LogDebug("New {Repo} created", nameof(BankAccountRepository));
    }

    public async Task<List<Guid>> AddOrUpdateAsync(Guid consentId, List<OpenBankingAccount> accounts)
    {
        _logger.LogInformation("Adding or updating {Count} accounts to consent #{ConsentId}", accounts.Count, consentId);
        var newRecords = new List<BankAccount>();

        await using var context = await _contextFactory.CreateDbContextAsync();

        var providerList = accounts.Select(a => a.AccountId);
        var accountsToUpdate = await context.BankAccounts.Where(a =>
            a.BankConsentId == consentId && providerList.Contains(a.ProviderAccountId))
            .ToListAsync();

        var accountsToCreate = accounts
            .Where(a => !accountsToUpdate.Select(b => b.ProviderAccountId).Contains(a.AccountId));

        // Update existing accounts
        accountsToUpdate.ForEach(a =>
        {
            _logger.LogDebug("Updating account {AccountID} for consent#{ConsentId}", a.BankAccountId, consentId);
            a.Updated = DateTime.UtcNow;
            context.BankAccounts.Update(a);
        });

        // Add new records
        foreach (var account in accountsToCreate)
        {
            _logger.LogDebug("Adding account {AccountID} to consent#{ConsentId}", account.AccountId, consentId);
            newRecords.Add(new BankAccount
            {
                BankConsentId = consentId,
                Updated = DateTime.UtcNow,
                ProviderAccountId = account.AccountId,
                AccountType = account.AccountType,
                DisplayName = account.DisplayName,
                Currency = account.Currency,
                SortCode = account.SortCode,
                AccountNumber = account.AccountNumber
            });
        }

        if (newRecords.Count > 1)
            await context.BankAccounts.AddRangeAsync(newRecords);

        await context.SaveChangesAsync();

        return newRecords
            .Select(a => a.BankAccountId).ToList()
            .Concat(accountsToUpdate.Select(a => a.BankAccountId))
            .ToList();
    }

    public async Task<OpenBankingAccount> GetAsync(Guid accountId)
    {
        await using var context = await _contextFactory.CreateDbContextAsync();

        var response = await context.BankAccounts
            .Include(a => a.BankConsent)
            .ThenInclude(c => c.BankingProvider)
            .FirstOrDefaultAsync(a => a.BankAccountId == accountId);
        if (response == default)
            throw new OpenBankingAccountNotFoundException(accountId.ToString());

        return BankAccountTranslator.ToModel(response, true);
    }

    
}