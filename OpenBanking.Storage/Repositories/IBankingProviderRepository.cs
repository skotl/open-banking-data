﻿namespace OpenBanking.Storage.Repositories;

public interface IBankingProviderRepository
{
    Task<Guid> CreateOrUpdateAsync(string providerId, string providerName, string providerLogoUri);
}