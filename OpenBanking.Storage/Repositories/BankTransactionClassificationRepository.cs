﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.Repositories;

internal class BankTransactionClassificationRepository : IBankTransactionClassificationRepository
{
    private static readonly Dictionary<string, Guid> LocalCache = new();
    private static readonly SemaphoreSlim CacheThrottler = new(1, 1);

    private readonly ILogger<BankTransactionClassificationRepository> _logger;
    private readonly IDbContextFactory<OpenBankingContext> _contextFactory;

    public BankTransactionClassificationRepository(
        ILogger<BankTransactionClassificationRepository> logger,
        IDbContextFactory<OpenBankingContext> contextFactory)
    {
        _logger = logger;
        _contextFactory = contextFactory;

        _logger.LogDebug("New {Repo} created", nameof(BankTransactionClassificationRepository));
    }

    public async Task<Guid> FindOrCreateAsync(string classification)
    {
        _logger.LogDebug("Find classification for {Classification}", classification);
        OpenBankingContext? context = default;

        await CacheThrottler.WaitAsync();
        try
        {
            if (LocalCache.Count == 0)
            {
                context = await _contextFactory.CreateDbContextAsync();
                await PopulateCache(context);
            }

            if (!LocalCache.ContainsKey(classification.ToLowerInvariant()))
            {
                context ??= await _contextFactory.CreateDbContextAsync();
                var id = await context.BankTransactionClassifications.AddAsync(
                    new BankTransactionClassification
                    {
                        Name = classification
                    });
                AddCacheEntry(classification, id.Entity.Id);
                await context.SaveChangesAsync();
            }
        }
        finally
        {
            if (context != default)
                await context.DisposeAsync();

            CacheThrottler.Release();
        }

        return LocalCache[classification.ToLowerInvariant()];
    }

    private static async Task<List<BankTransactionClassification>> GetAllAsync(OpenBankingContext context)
    {
        return await context.BankTransactionClassifications.ToListAsync();
    }

    private static async Task PopulateCache(OpenBankingContext context)
    {
        (await GetAllAsync(context))
            .ForEach(c => AddCacheEntry(c.Name, c.Id));
    }

    private static void AddCacheEntry(string classification, Guid id)
    {
        LocalCache.Add(classification.ToLowerInvariant(), id);
    }
}