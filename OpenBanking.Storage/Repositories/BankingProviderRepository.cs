﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.Repositories;

internal class BankingProviderRepository : IBankingProviderRepository
{
    private readonly ILogger<BankingProviderRepository> _logger;
    private readonly IDbContextFactory<OpenBankingContext> _contextFactory;

    public BankingProviderRepository(ILogger<BankingProviderRepository> logger, IDbContextFactory<OpenBankingContext> contextFactory)
    {
        _logger = logger;
        _contextFactory = contextFactory;

        _logger.LogDebug("New {Repo} created", nameof(BankingProviderRepository));
    }

    public async Task<Guid> CreateOrUpdateAsync(string providerId, string providerName, string providerLogoUri)
    {
        _logger.LogDebug("Checking whether banking provider for {ProviderName} needs to be created", providerName);

        await using var context = await _contextFactory.CreateDbContextAsync();

        // If not exist, create it
        var provider = await context.BankingProviders.FirstOrDefaultAsync(bp => bp.OpenBankingProviderId == providerId);
        if (provider == null)
        {
            provider = new BankingProvider { OpenBankingProviderId = providerId, Name = providerName, LogoUri = providerLogoUri };
            await context.BankingProviders.AddAsync(provider);
            await context.SaveChangesAsync();
        }

        // If does exist and we have a logo that differs, then update the logo
        // ReSharper disable once InvertIf
        if (!string.IsNullOrWhiteSpace(providerLogoUri) &&
            (string.IsNullOrWhiteSpace(provider.LogoUri) ||
             string.Compare(providerLogoUri, provider.LogoUri, StringComparison.InvariantCultureIgnoreCase) != 0))
        {
            provider.LogoUri = providerLogoUri;
            await context.SaveChangesAsync();
        }

        return provider.BankingProviderId;
    }
}