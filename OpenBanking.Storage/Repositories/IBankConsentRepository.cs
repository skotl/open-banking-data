﻿using OpenBanking.Models;

namespace OpenBanking.Storage.Repositories;

public interface IBankConsentRepository
{
    /// <summary>
    /// Add a bank consent
    /// </summary>
    /// <param name="bankProviderId"></param>
    /// <param name="userId"></param>
    /// <param name="openBankingConnection"></param>
    /// <returns></returns>
    Task<Guid> Add(Guid bankProviderId, string userId, OpenBankingConnection openBankingConnection);
    
    /// <summary>
    /// Delete a bank consent
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="bankConsentId"></param>
    /// <returns></returns>
    Task DeleteConnectionAsync(string userId, Guid bankConsentId);

    /// <summary>
    ///  Get access token for the given account ID, renewing if appropriate
    /// </summary>
    /// <param name="bankAccountId"></param>
    /// <returns></returns>
    Task<string> GetAccessTokenForBankAccount(Guid bankAccountId);

    /// <summary>
    /// If the user already has a consent for this provider then fetch that, otherwise
    /// return null
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="providerId"></param>
    /// <returns></returns>
    Task<OpenBankingConsent?> GetForProviderAsync(string userId, Guid providerId);

    /// <summary>
    /// Update an existing consent record
    /// </summary>
    /// <param name="consent"></param>
    /// <returns></returns>
    Task UpdateAsync(OpenBankingConsent consent);
}