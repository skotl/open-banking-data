﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBanking.Abstract.Exceptions;
using OpenBanking.Models;
using OpenBanking.Storage.DtoTranslation;
using OpenBanking.Storage.Entities;
using OpenBanking.Storage.Exceptions;

namespace OpenBanking.Storage.Repositories;

internal class ScheduledTaskRepository(
    ILogger<ScheduledTaskRepository> logger,
    IDbContextFactory<OpenBankingContext> contextFactory)
    : IScheduledTaskRepository
{
    private const int PollRequestDelaySeconds = 30;

    public async Task<Guid> AddTransactionsRequestJobAsync(Guid? accountId, string description, DateTime from, DateTime to)
    {
        logger.LogInformation("Adding request to fetch transactions - {Desc}", description);

        await using var context = await contextFactory.CreateDbContextAsync() ;
            
        var job = new ScheduledTask
        {
            TaskType = (int) OpenBankingScheduledTask.JobTypes.GetTransactions,
            Description = description,
            BankAccountId = accountId,
            Created = DateTime.UtcNow,
            DelayStartUntil = DateTime.UtcNow,
            DataFrom = from,
            DataTo = to,
            MaxRetries = 0
        };

        await context.ScheduledTasks.AddAsync(job);
        await context.SaveChangesAsync();

        return job.ScheduledTaskId;
    }

    public async Task<Guid> AddPollForTransactionsRequestAsync(Guid? accountId, string description, string pollUri)
    {
        logger.LogInformation("Adding request to poll for transactions - {Desc}", description);

        if (string.IsNullOrWhiteSpace(pollUri))
            throw new ArgumentException(null, nameof(pollUri));

        await using var context = await contextFactory.CreateDbContextAsync() ;
        
        var job = new ScheduledTask
        {
            TaskType = (int) OpenBankingScheduledTask.JobTypes.PollForTransactions,
            Description = description,
            BankAccountId = accountId,
            Created = DateTime.UtcNow,
            DelayStartUntil = DateTime.UtcNow.AddSeconds(PollRequestDelaySeconds),
            DataPollUri = pollUri,
            MaxRetries = 5
        };

        await context.ScheduledTasks.AddAsync(job);
        await context.SaveChangesAsync();

        return job.ScheduledTaskId;
    }

    public async Task DeferJob(Guid id, int deferSecondsMultiple)
    {
        logger.LogInformation("Defer job #{Id} for {Delay} seconds", id, deferSecondsMultiple);

        await using var context = await contextFactory.CreateDbContextAsync() ;

        var job = await context.ScheduledTasks.FirstOrDefaultAsync(t => t.ScheduledTaskId == id);
        if (job == default)
            throw new ScheduledTaskNotFoundException(id.ToString());

        if (++job.RetryCount > job.MaxRetries)
            throw new OpenBankingScheduledTaskException($"Job #{id} has exceeded the max retry count of {job.MaxRetries}");

        job.DelayStartUntil = DateTime.UtcNow.AddSeconds(deferSecondsMultiple * job.RetryCount);
        await context.SaveChangesAsync();
    }

    public async Task MarkJobInProgressAsync(Guid id)
    {
        logger.LogInformation("Marking in progress for #{Id}", id);

        await using var context = await contextFactory.CreateDbContextAsync() ;

        var job = await context.ScheduledTasks.FirstOrDefaultAsync(t => t.ScheduledTaskId == id);
        if (job == default)
            throw new ScheduledTaskNotFoundException(id.ToString());

        job.InProgress = true;
        await context.SaveChangesAsync();
    }

    public async Task ClearJobInProgressAsync(Guid id)
    {
        logger.LogInformation("Clearing in progress for #{Id}", id);

        await using var context = await contextFactory.CreateDbContextAsync() ;

        var job = await context.ScheduledTasks.FirstOrDefaultAsync(t => t.ScheduledTaskId == id);
        if (job == default)
            throw new ScheduledTaskNotFoundException(id.ToString());

        job.InProgress = false;
        await context.SaveChangesAsync();
    }

    public async Task<List<OpenBankingScheduledTask>> GetOutstandingJobsAsync()
    {
        await using var context = await contextFactory.CreateDbContextAsync() ;

        return await context.ScheduledTasks.Where(t =>
                !t.InProgress && DateTime.UtcNow > t.DelayStartUntil)
            .Select(t => ScheduledTaskTranslator.From(t))
            .ToListAsync();
    }

    public async Task RemoveErroredJob(Guid jobId, string message)
    {
        logger.LogInformation("Removing errored task #{Id}", jobId);

        await using var context = await contextFactory.CreateDbContextAsync() ;

        var task = await context.ScheduledTasks.FirstOrDefaultAsync(t => t.ScheduledTaskId == jobId);

        if (task == default)
            throw new ScheduledTaskNotFoundException(jobId.ToString());

        var info = new ScheduledTaskMessage
        {
            OriginalTaskId = task.ScheduledTaskId,
            TaskType = task.TaskType,
            Description = task.Description,
            BankAccountId = task.BankAccountId,
            Created = DateTime.UtcNow,
            Message = message
        };

        await context.ScheduledTaskMessages.AddAsync(info);
        context.ScheduledTasks.Remove(task);
        await context.SaveChangesAsync();
    }

    public async Task RemoveIfNotDeferred(Guid jobId, string message)
    {
        logger.LogInformation("Removing scheduled task #{Id} (if not deferred)", jobId);

        await using var context = await contextFactory.CreateDbContextAsync() ;

        var task = await context.ScheduledTasks.FirstOrDefaultAsync(t => t.ScheduledTaskId == jobId);

        if (task == default)
            throw new ScheduledTaskNotFoundException(jobId.ToString());

        if (task.DelayStartUntil > DateTime.UtcNow)
        {
            logger.LogInformation("Scheduled task is deferred until {Time}, so not deleting", task.DelayStartUntil);
            return;
        }

        var info = new ScheduledTaskMessage
        {
            OriginalTaskId = task.ScheduledTaskId,
            TaskType = task.TaskType,
            Description = task.Description,
            BankAccountId = task.BankAccountId,
            Created = DateTime.UtcNow,
            Message = message
        };

        await context.ScheduledTaskMessages.AddAsync(info);
        context.ScheduledTasks.Remove(task);
        await context.SaveChangesAsync();
    }

    public async Task RemoveAllAsync()
    {
        await using var context = await contextFactory.CreateDbContextAsync() ;

        await context.ScheduledTasks.ExecuteDeleteAsync();

        await context.SaveChangesAsync();
    }
}