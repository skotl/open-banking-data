﻿using OpenBanking.Models;

namespace OpenBanking.Storage.Repositories;

public interface IBankTransactionRepository
{
    Task InsertOrUpdateAsync(Guid accountId, string providerAccountId, List<OpenBankingTransaction> transactions);
}