﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenBanking.Abstract;
using OpenBanking.Abstract.Exceptions;
using OpenBanking.Models;
using OpenBanking.Storage.DtoTranslation;
using OpenBanking.Storage.Entities;
using OpenBanking.Storage.Exceptions;

namespace OpenBanking.Storage.Repositories;

internal class BankConsentRepository : IBankConsentRepository
{
    private readonly ILogger<BankConsentRepository> _logger;
    private readonly IDbContextFactory<OpenBankingContext> _contextFactory;
    private readonly IOpenBankingAuthentication _openBankingAuthentication;
    private readonly OpenBankingConfiguration _config;

    /// <summary>
    /// Multiple tasks / threads might try to refresh the access token at roughly the same time
    /// so ensure we throttle that request to ensure one request at a time
    /// </summary>
    private static readonly SemaphoreSlim AccessTokenThrottler = new(1, 1);

    public BankConsentRepository(
        ILogger<BankConsentRepository> logger,
        IDbContextFactory<OpenBankingContext> contextFactory,
        IOptions<OpenBankingConfiguration> options,
        IOpenBankingAuthentication openBankingAuthentication)
    {
        _logger = logger;
        _contextFactory = contextFactory;
        _openBankingAuthentication = openBankingAuthentication;
        _config = options.Value;

        _logger.LogDebug("New {Repo} created", nameof(BankConsentRepository));
    }

    public async Task<Guid> Add(Guid bankProviderId, string userId, OpenBankingConnection openBankingConnection)
    {
        _logger.LogInformation("Adding consent for {ProviderId} to user#{UserId}", bankProviderId, userId);

        await using var context = await _contextFactory.CreateDbContextAsync();

        var record = new BankConsent
        {
            UserAccountId = userId,
            AccessToken = openBankingConnection.Consent.AccessToken,
            BankingProviderId = bankProviderId,
            UserFullName = openBankingConnection.UserFullName,
            ConsentExpires = openBankingConnection.Consent.ConsentExpires,
            ConsentGranted = openBankingConnection.Consent.ConsentGranted,
            RefreshToken = openBankingConnection.Consent.RefreshToken,
            RefreshTokenExpires = openBankingConnection.Consent.RefreshTokenExpires,
            TokenExpiryUtc = openBankingConnection.Consent.TokenExpiryUtc
        };

        await context.BankConsents.AddAsync(record);
        await context.SaveChangesAsync();

        return record.BankConsentId;
    }

    public async Task DeleteConnectionAsync(string userId, Guid bankConsentId)
    {
        _logger.LogInformation("Delete consent for user#{UserId}, #{BankConsentId}", userId, bankConsentId);

        await using var context = await _contextFactory.CreateDbContextAsync();

        var response = await GetConsentRecordOrThrow(userId, bankConsentId, context);

        context.Remove(response);
        await context.SaveChangesAsync();
    }

    /// <summary>
    /// Get the current access token, renewing where applicable (and possible), from a bank account ID
    /// </summary>
    /// <param name="bankAccountId"></param>
    /// <returns></returns>
    /// <exception cref="OpenBankingConsentHasExpiredException">Consent has expired and cannot be renewed</exception>
    public async Task<string> GetAccessTokenForBankAccount(Guid bankAccountId)
    {
        // Ensure only one thread at a time can attempt to read the access token
        await AccessTokenThrottler.WaitAsync();
        try
        {
            await using var context = await _contextFactory.CreateDbContextAsync();

            var response = await context.BankAccounts
                .Include(a => a.BankConsent)
                .FirstOrDefaultAsync(a => a.BankAccountId == bankAccountId);

            if (response == default)
                throw new OpenBankingAccountNotFoundException(bankAccountId.ToString());

            return await GetAccessTokenForBankConsent(response.BankConsent.UserAccountId, response.BankConsentId,
                context);
        }
        finally
        {
            AccessTokenThrottler.Release();
        }
    }

    public async Task<OpenBankingConsent?> GetForProviderAsync(string userId, Guid providerId)
    {
        _logger.LogDebug("Get for user={UserId}, provider={ProviderId}", userId, providerId);

        await using var context = await _contextFactory.CreateDbContextAsync();

        var dto = await context.BankConsents.FirstOrDefaultAsync(c =>
            c.UserAccountId == userId && c.BankingProviderId == providerId);

        return dto == null ? null : BankConsentTranslator.ToModel(dto);
    }

    public async Task UpdateAsync(OpenBankingConsent model)
    {
        _logger.LogInformation("Update consent #{Id}", model.Id);

        if (model.Id == default)
            throw new ArgumentException("Trying to update an unsaved (new) record");

        await using var context = await _contextFactory.CreateDbContextAsync();

        var dto = await context.BankConsents.FirstOrDefaultAsync(c => c.BankConsentId == model.Id);
        if (dto == default)
            throw new OpenBankingConsentNotFoundException(model.Id.ToString());

        // As the model is not fully populated we only support updating a few properties
        dto.AccessToken = model.AccessToken;
        dto.RefreshToken = model.RefreshToken;
        dto.ConsentGranted = model.ConsentGranted;
        dto.ConsentExpires = model.ConsentExpires;
        dto.RefreshTokenExpires = model.RefreshTokenExpires;
        dto.TokenExpiryUtc = model.TokenExpiryUtc;

        context.Update(dto);
        await context.SaveChangesAsync();
    }

    /// <summary>
    /// Get the current access token, renewing where applicable (and possible), from a bank consent ID
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="bankConsentId"></param>
    /// <param name="context"></param>
    /// <returns></returns>
    /// <exception cref="OpenBankingConsentHasExpiredException">Consent has expired and cannot be renewed</exception>
    private async Task<string> GetAccessTokenForBankConsent(string userId, Guid bankConsentId, OpenBankingContext context)
    {
        _logger.LogDebug("Requesting access token for user#{UserId}, #{BankConsentId}", userId, bankConsentId);

        var response = await GetConsentRecordOrThrow(userId, bankConsentId, context);

        if (response.ConsentExpires < DateTime.UtcNow)
            throw new OpenBankingConsentHasExpiredException(
                $"Consent #{bankConsentId} expired at {response.ConsentExpires} and cannot be renewed");

        if (response.TokenExpiryUtc > DateTime.UtcNow)
            return response.AccessToken;

        _logger.LogInformation("Refreshing access token for user#{UserId}, #{BankConsentId}", userId, bankConsentId);
        var newJwt = await _openBankingAuthentication.RefreshAccessTokenAsync(userId, _config.ClientId,
            _config.ClientSecret,
            response.RefreshToken);

        response.AccessToken = newJwt.AccessToken;
        response.TokenExpiryUtc = newJwt.ExpiryUtc;
        response.RefreshToken = newJwt.RefreshToken;

        await context.SaveChangesAsync();
        return response.AccessToken;
    }

    private static async Task<BankConsent> GetConsentRecordOrThrow(
        string userId,
        Guid bankConsentId,
        OpenBankingContext context)
    {
        var response = await context.BankConsents
            .FirstOrDefaultAsync(c => c.BankConsentId == bankConsentId && c.UserAccountId == userId);
        if (response == default)
            throw new BankConsentNotFoundException(bankConsentId.ToString());

        return response;
    }
}