﻿using OpenBanking.Models;

namespace OpenBanking.Storage.Repositories;

public interface IUserRepository
{
    Task CreateIfNotExistsAsync(string userId);

    Task<OpenBankingUserDetails> GetAsync(string userId);
}