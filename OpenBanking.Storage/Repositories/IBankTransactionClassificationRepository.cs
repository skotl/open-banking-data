﻿namespace OpenBanking.Storage.Repositories;

public interface IBankTransactionClassificationRepository
{
    Task<Guid> FindOrCreateAsync(string classification);
}