﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OpenBanking.Models;
using OpenBanking.Storage.DtoTranslation;
using OpenBanking.Storage.Entities;
using OpenBanking.Storage.Exceptions;

namespace OpenBanking.Storage.Repositories;

internal class UserRepository : IUserRepository
{
    private readonly ILogger<UserRepository> _logger;
    private readonly IDbContextFactory<OpenBankingContext> _contextFactory;

    public UserRepository(ILogger<UserRepository> logger, IDbContextFactory<OpenBankingContext> contextFactory)
    {
        _logger = logger;
        _contextFactory = contextFactory;

        _logger.LogDebug("New {Repo} created", nameof(UserRepository));
    }

    public async Task CreateIfNotExistsAsync(string userId)
    {
        _logger.LogDebug("Verifying that user {UserId} exists", userId);

        await using var context = await _contextFactory.CreateDbContextAsync();

        var response = await context.UserAccounts.FirstOrDefaultAsync(u => u.UserAccountId == userId);
        if (response != null)
            return;

        await context.UserAccounts.AddAsync(new UserAccount { UserAccountId = userId });
        await context.SaveChangesAsync();
    }

    public async Task<OpenBankingUserDetails> GetAsync(string userId)
    {
        _logger.LogDebug("Fetching user #{UserId}", userId);

        await using var context = await _contextFactory.CreateDbContextAsync();

        var user = await context.UserAccounts
            .Include(u => u.BankConsents)
            .ThenInclude(c => c.BankingProvider)
            .Include(u => u.BankConsents)
            .ThenInclude(c => c.BankAccounts)
            .FirstOrDefaultAsync(u => u.UserAccountId == userId);

        if (user == null)
            throw new UserNotFoundException(userId);

        return UserDetailsTranslator.ToModel(user, true);
    }
}