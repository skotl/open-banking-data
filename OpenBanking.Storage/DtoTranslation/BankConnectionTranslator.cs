﻿using OpenBanking.Models;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.DtoTranslation;

internal static class BankConnectionTranslator
{
    public static OpenBankingConnection ToModel(BankConsent consent, bool enforceStructure)
    {
        ArgumentNullException.ThrowIfNull(consent);
        if (enforceStructure)
            ValidateRelationsOrThrow(consent);

        return new OpenBankingConnection
        {
            Id = consent.BankConsentId,
            UserFullName = consent.UserFullName,
            Provider = consent.BankingProvider.Name,
            ProviderLogoUri = consent.BankingProvider.LogoUri,
            Consent = new OpenBankingConsent { ConsentGranted = consent.ConsentGranted },
            Accounts = PopulateAccounts(consent.BankAccounts, enforceStructure)
        };
    }

    private static List<OpenBankingAccount> PopulateAccounts(List<BankAccount>? consentBankAccounts,
        bool enforceStructure)
    {
        var response = new List<OpenBankingAccount>();

        if (consentBankAccounts is { Count: > 0 })
            response.AddRange(consentBankAccounts.Select(a => BankAccountTranslator.ToModel(a, enforceStructure)));

        return response;
    }

    private static void ValidateRelationsOrThrow(BankConsent consent)
    {
        if (consent.BankingProvider == null)
            throw new InvalidDataException($"connection#{consent.BankConsentId}, {nameof(consent.BankingProvider)}");
        if (consent.UserAccount == null)
            throw new InvalidDataException($"connection#{consent.BankConsentId}, {nameof(consent.UserAccount)}");
        if (consent.BankAccounts == null)
            throw new InvalidDataException($"connection#{consent.BankConsentId}, {nameof(consent.BankAccounts)}");
    }
}