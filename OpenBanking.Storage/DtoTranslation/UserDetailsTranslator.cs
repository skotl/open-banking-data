﻿using OpenBanking.Models;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.DtoTranslation;

internal static class UserDetailsTranslator
{
    public static OpenBankingUserDetails ToModel(UserAccount account, bool enforceStructure)
    {
        ArgumentNullException.ThrowIfNull(account);
        if (enforceStructure)
            ValidateRelationsOrThrow(account);

        return new OpenBankingUserDetails
        {
            UserId = account.UserAccountId,
            BankConnections = PopulateConnections(account.BankConsents, enforceStructure)
        };
    }

    private static List<OpenBankingConnection> PopulateConnections(List<BankConsent>? consents, bool enforceStructure)
    {
        var response = new List<OpenBankingConnection>();

        if (consents is { Count: > 0 })
            response.AddRange(consents.Select(c => BankConnectionTranslator.ToModel(c, enforceStructure)));
        
        return response;
    }

    private static void ValidateRelationsOrThrow(UserAccount account)
    {
        if (account.BankConsents == null)
            throw new InvalidDataException($"user#{account.UserAccountId}, {nameof(account.BankConsents)}");
    }
}