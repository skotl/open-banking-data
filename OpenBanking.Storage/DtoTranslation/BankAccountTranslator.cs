﻿using OpenBanking.Models;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.DtoTranslation;

internal static class BankAccountTranslator
{
    public static OpenBankingAccount ToModel(BankAccount account, bool enforceStructure)
    {
        ArgumentNullException.ThrowIfNull(account);
        if (enforceStructure)
            ValidateRelationsOrThrow(account);

        return new OpenBankingAccount
        {
            Id = account.BankAccountId,
            Updated = account.Updated,
            AccountId = account.ProviderAccountId,
            AccountType = account.AccountType,
            DisplayName = account.DisplayName,
            Currency = account.Currency,
            AccountNumber = account.AccountNumber,
            SortCode = account.SortCode,
            ProviderName = account.BankConsent.BankingProvider.Name,
            ProviderId = account.BankConsent.BankingProvider.OpenBankingProviderId,
            LogoUri = account.BankConsent.BankingProvider.LogoUri
        };
    }

    private static void ValidateRelationsOrThrow(BankAccount account)
    {
        if (account.BankConsent == null)
            throw new InvalidDataException($"account#{account.BankAccountId}, {nameof(account.BankConsent)}");
        if (account.BankConsent.BankingProvider == null)
            throw new InvalidDataException(
                $"account#{account.BankAccountId}, {nameof(account.BankConsent.BankingProvider)}");
    }
}