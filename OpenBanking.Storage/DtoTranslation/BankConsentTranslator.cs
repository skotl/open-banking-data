﻿using OpenBanking.Models;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.DtoTranslation;

internal static class BankConsentTranslator
{
    public static OpenBankingConsent ToModel(BankConsent consent)
    {
        ArgumentNullException.ThrowIfNull(consent);

        return new OpenBankingConsent
        {
            Id = consent.BankConsentId,
            AccessToken = consent.AccessToken,
            RefreshToken = consent.RefreshToken,
            ConsentGranted = consent.ConsentGranted,
            RefreshTokenExpires = consent.RefreshTokenExpires,
            TokenExpiryUtc = consent.TokenExpiryUtc
        };
    }
}