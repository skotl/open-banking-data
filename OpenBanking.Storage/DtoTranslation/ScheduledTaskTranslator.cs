﻿using OpenBanking.Models;
using OpenBanking.Storage.Entities;

namespace OpenBanking.Storage.DtoTranslation;

internal static class ScheduledTaskTranslator
{
    public static OpenBankingScheduledTask From(ScheduledTask dto)
    {
        var model = new OpenBankingScheduledTask
        {
            Id = dto.ScheduledTaskId,
            TaskType = dto.TaskType,
            Description = dto.Description,
            BankAccountId = dto.BankAccountId,
            Created = dto.Created,
            DelayStartUntil = dto.DelayStartUntil,
            LastAttemptStartedAt = dto.LastAttemptStartedAt,
            LastAttemptCompletedAt = dto.LastAttemptCompletedAt,
            InProgress = dto.InProgress,
            RetryCount = dto.RetryCount,
            MaxRetries = dto.MaxRetries,
            Error = dto.Error,
            Data =
            {
                PollUri = dto.DataPollUri,
                FromDate = dto.DataFrom,
                ToDate = dto.DataTo
            }
        };

        return model;
    }

    public static ScheduledTask From(OpenBankingScheduledTask model)
    {
        return new ScheduledTask
        {
            ScheduledTaskId = model.Id,
            TaskType = model.TaskType,
            Description = model.Description,
            BankAccountId = model.BankAccountId,
            Created = model.Created,
            DataFrom = model.Data.FromDate,
            DataTo = model.Data.ToDate,
            DataPollUri = model.Data.PollUri,
            DelayStartUntil = model.DelayStartUntil,
            LastAttemptStartedAt = model.LastAttemptStartedAt,
            LastAttemptCompletedAt = model.LastAttemptCompletedAt,
            InProgress = model.InProgress,
            RetryCount = model.RetryCount,
            MaxRetries = model.MaxRetries,
            Error = model.Error
        };
    }
}