﻿using Microsoft.EntityFrameworkCore;
using OpenBanking.Storage.Entities;


namespace OpenBanking.Storage;

// ReSharper disable PropertyCanBeMadeInitOnly.Global
// Reason: Set by Entity Framework
internal class OpenBankingContext : DbContext
{

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public OpenBankingContext(DbContextOptions<OpenBankingContext> options)
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        : base(options)
    {

    }

    public DbSet<BankAccount> BankAccounts { get; set; }
    public DbSet<BankConsent> BankConsents { get; set; }
    public DbSet<BankingProvider> BankingProviders { get; set; }
    public DbSet<BankTransaction> BankTransactions { get; set; }
    public DbSet<BankTransactionClassification> BankTransactionClassifications { get; set; }
    public DbSet<Currency> Currencies { get; set; }
    public DbSet<UserAccount> UserAccounts { get; set; }
    public DbSet<ScheduledTask> ScheduledTasks { get; set; }
    public DbSet<ScheduledTaskMessage> ScheduledTaskMessages { get; set; }
}