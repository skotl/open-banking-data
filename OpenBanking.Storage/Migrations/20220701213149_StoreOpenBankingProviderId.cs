﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenBanking.Storage.Migrations
{
    public partial class StoreOpenBankingProviderId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BankingProviders_Name",
                table: "BankingProviders");

            migrationBuilder.AddColumn<string>(
                name: "OpenBankingProviderId",
                table: "BankingProviders",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_BankingProviders_Name",
                table: "BankingProviders",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_BankingProviders_OpenBankingProviderId",
                table: "BankingProviders",
                column: "OpenBankingProviderId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BankingProviders_Name",
                table: "BankingProviders");

            migrationBuilder.DropIndex(
                name: "IX_BankingProviders_OpenBankingProviderId",
                table: "BankingProviders");

            migrationBuilder.DropColumn(
                name: "OpenBankingProviderId",
                table: "BankingProviders");

            migrationBuilder.CreateIndex(
                name: "IX_BankingProviders_Name",
                table: "BankingProviders",
                column: "Name",
                unique: true);
        }
    }
}
