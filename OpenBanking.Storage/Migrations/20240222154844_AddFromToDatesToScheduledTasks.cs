﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenBanking.Storage.Migrations
{
    public partial class AddFromToDatesToScheduledTasks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DataFrom",
                table: "ScheduledTasks",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DataTo",
                table: "ScheduledTasks",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ScheduledTasks",
                type: "nvarchar(256)",
                maxLength: 256,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ScheduledTaskMessages",
                type: "nvarchar(256)",
                maxLength: 256,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataFrom",
                table: "ScheduledTasks");

            migrationBuilder.DropColumn(
                name: "DataTo",
                table: "ScheduledTasks");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ScheduledTasks");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ScheduledTaskMessages");
        }
    }
}
