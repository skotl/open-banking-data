﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenBanking.Storage.Migrations
{
    /// <inheritdoc />
    public partial class AddTransactionClassifications : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "PrimaryClassificationId",
                table: "BankTransactions",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SecondaryClassificationId",
                table: "BankTransactions",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BankTransactionClassifications",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankTransactionClassifications", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankTransactions_PrimaryClassificationId_Id",
                table: "BankTransactions",
                columns: new[] { "PrimaryClassificationId", "Id" },
                unique: true,
                filter: "[PrimaryClassificationId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BankTransactions_PrimaryClassificationId_SecondaryClassificationId_Id",
                table: "BankTransactions",
                columns: new[] { "PrimaryClassificationId", "SecondaryClassificationId", "Id" },
                unique: true,
                filter: "[PrimaryClassificationId] IS NOT NULL AND [SecondaryClassificationId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BankTransactions_SecondaryClassificationId_Id",
                table: "BankTransactions",
                columns: new[] { "SecondaryClassificationId", "Id" },
                unique: true,
                filter: "[SecondaryClassificationId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BankTransactionClassifications_Name",
                table: "BankTransactionClassifications",
                column: "Name",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BankTransactions_BankTransactionClassifications_PrimaryClassificationId",
                table: "BankTransactions",
                column: "PrimaryClassificationId",
                principalTable: "BankTransactionClassifications",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BankTransactions_BankTransactionClassifications_SecondaryClassificationId",
                table: "BankTransactions",
                column: "SecondaryClassificationId",
                principalTable: "BankTransactionClassifications",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankTransactions_BankTransactionClassifications_PrimaryClassificationId",
                table: "BankTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_BankTransactions_BankTransactionClassifications_SecondaryClassificationId",
                table: "BankTransactions");

            migrationBuilder.DropTable(
                name: "BankTransactionClassifications");

            migrationBuilder.DropIndex(
                name: "IX_BankTransactions_PrimaryClassificationId_Id",
                table: "BankTransactions");

            migrationBuilder.DropIndex(
                name: "IX_BankTransactions_PrimaryClassificationId_SecondaryClassificationId_Id",
                table: "BankTransactions");

            migrationBuilder.DropIndex(
                name: "IX_BankTransactions_SecondaryClassificationId_Id",
                table: "BankTransactions");

            migrationBuilder.DropColumn(
                name: "PrimaryClassificationId",
                table: "BankTransactions");

            migrationBuilder.DropColumn(
                name: "SecondaryClassificationId",
                table: "BankTransactions");
        }
    }
}
