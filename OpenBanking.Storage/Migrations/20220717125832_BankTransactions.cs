﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenBanking.Storage.Migrations
{
    public partial class BankTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BankTransactions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    AccountId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProviderTransactionId = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TransactionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: true),
                    TransactionType = table.Column<int>(type: "int", nullable: false),
                    Category = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Merchant = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    CurrencyId = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    RunningBalanceCurrencyId = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: true),
                    RunningBalanceAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BankTransactions_BankAccounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "BankAccounts",
                        principalColumn: "BankAccountId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankTransactions_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankTransactions_Currencies_RunningBalanceCurrencyId",
                        column: x => x.RunningBalanceCurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Code");
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankTransactions_AccountId_ProviderTransactionId",
                table: "BankTransactions",
                columns: new[] { "AccountId", "ProviderTransactionId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BankTransactions_AccountId_TransactionTime",
                table: "BankTransactions",
                columns: new[] { "AccountId", "TransactionTime" });

            migrationBuilder.CreateIndex(
                name: "IX_BankTransactions_CurrencyId",
                table: "BankTransactions",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_BankTransactions_RunningBalanceCurrencyId",
                table: "BankTransactions",
                column: "RunningBalanceCurrencyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankTransactions");
        }
    }
}
