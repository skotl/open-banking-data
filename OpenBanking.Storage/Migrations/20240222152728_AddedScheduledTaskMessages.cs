﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenBanking.Storage.Migrations
{
    public partial class AddedScheduledTaskMessages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ScheduledTaskMessages",
                columns: table => new
                {
                    ScheduledTaskMessageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    TaskType = table.Column<int>(type: "int", nullable: false),
                    BankAccountId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Message = table.Column<string>(type: "nvarchar(max)", maxLength: 4096, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduledTaskMessages", x => x.ScheduledTaskMessageId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledTaskMessages_BankAccountId_Created",
                table: "ScheduledTaskMessages",
                columns: new[] { "BankAccountId", "Created" });

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledTaskMessages_Created",
                table: "ScheduledTaskMessages",
                column: "Created");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ScheduledTaskMessages");
        }
    }
}
