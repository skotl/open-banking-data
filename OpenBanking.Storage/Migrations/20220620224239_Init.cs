﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenBanking.Storage.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BankingProviders",
                columns: table => new
                {
                    BankingProviderId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    LogoUri = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankingProviders", x => x.BankingProviderId);
                });

            migrationBuilder.CreateTable(
                name: "UserAccounts",
                columns: table => new
                {
                    UserAccountId = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccounts", x => x.UserAccountId);
                });

            migrationBuilder.CreateTable(
                name: "BankConsents",
                columns: table => new
                {
                    BankConsentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    UserAccountId = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    BankingProviderId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserFullName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    AccessToken = table.Column<string>(type: "nvarchar(max)", maxLength: 8192, nullable: false),
                    RefreshToken = table.Column<string>(type: "nvarchar(max)", maxLength: 8192, nullable: false),
                    ConsentGranted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ConsentExpires = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RefreshTokenExpires = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TokenExpiryUtc = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankConsents", x => x.BankConsentId);
                    table.ForeignKey(
                        name: "FK_BankConsents_BankingProviders_BankingProviderId",
                        column: x => x.BankingProviderId,
                        principalTable: "BankingProviders",
                        principalColumn: "BankingProviderId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BankConsents_UserAccounts_UserAccountId",
                        column: x => x.UserAccountId,
                        principalTable: "UserAccounts",
                        principalColumn: "UserAccountId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BankAccounts",
                columns: table => new
                {
                    BankAccountId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    BankConsentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ProviderAccountId = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    AccountType = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    DisplayName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Currency = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    SortCode = table.Column<string>(type: "nvarchar(8)", maxLength: 8, nullable: true),
                    AccountNumber = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankAccounts", x => x.BankAccountId);
                    table.ForeignKey(
                        name: "FK_BankAccounts_BankConsents_BankConsentId",
                        column: x => x.BankConsentId,
                        principalTable: "BankConsents",
                        principalColumn: "BankConsentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankAccounts_BankConsentId_ProviderAccountId",
                table: "BankAccounts",
                columns: new[] { "BankConsentId", "ProviderAccountId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BankConsents_BankingProviderId",
                table: "BankConsents",
                column: "BankingProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_BankConsents_UserAccountId",
                table: "BankConsents",
                column: "UserAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_BankingProviders_Name",
                table: "BankingProviders",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankAccounts");

            migrationBuilder.DropTable(
                name: "BankConsents");

            migrationBuilder.DropTable(
                name: "BankingProviders");

            migrationBuilder.DropTable(
                name: "UserAccounts");
        }
    }
}
