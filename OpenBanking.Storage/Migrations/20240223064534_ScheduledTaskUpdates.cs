﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenBanking.Storage.Migrations
{
    public partial class ScheduledTaskUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DataPollUri",
                table: "ScheduledTasks",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OriginalTaskId",
                table: "ScheduledTaskMessages",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledTaskMessages_OriginalTaskId_Created",
                table: "ScheduledTaskMessages",
                columns: new[] { "OriginalTaskId", "Created" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ScheduledTaskMessages_OriginalTaskId_Created",
                table: "ScheduledTaskMessages");

            migrationBuilder.DropColumn(
                name: "DataPollUri",
                table: "ScheduledTasks");

            migrationBuilder.DropColumn(
                name: "OriginalTaskId",
                table: "ScheduledTaskMessages");
        }
    }
}
