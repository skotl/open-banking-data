﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenBanking.Storage.Migrations
{
    public partial class AddDelayStartToScheduledJobs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ScheduledTasks_RetryAfter",
                table: "ScheduledTasks");

            migrationBuilder.DropColumn(
                name: "RetryAfter",
                table: "ScheduledTasks");

            migrationBuilder.AddColumn<DateTime>(
                name: "DelayStartUntil",
                table: "ScheduledTasks",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledTasks_DelayStartUntil",
                table: "ScheduledTasks",
                column: "DelayStartUntil");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ScheduledTasks_DelayStartUntil",
                table: "ScheduledTasks");

            migrationBuilder.DropColumn(
                name: "DelayStartUntil",
                table: "ScheduledTasks");

            migrationBuilder.AddColumn<DateTime>(
                name: "RetryAfter",
                table: "ScheduledTasks",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledTasks_RetryAfter",
                table: "ScheduledTasks",
                column: "RetryAfter");
        }
    }
}
