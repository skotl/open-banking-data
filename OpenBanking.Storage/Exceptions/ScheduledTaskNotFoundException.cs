﻿namespace OpenBanking.Storage.Exceptions;

public class ScheduledTaskNotFoundException : Exception
{
    public ScheduledTaskNotFoundException(string msg) : base(msg)
    {}
}