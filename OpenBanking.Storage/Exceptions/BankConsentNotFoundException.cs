﻿namespace OpenBanking.Storage.Exceptions;

public class BankConsentNotFoundException : Exception
{
    public BankConsentNotFoundException(string msg) : base(msg)
    {}
}