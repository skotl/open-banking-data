A sample application that shows how to access account data via (UK) Open Banking APIs.

This solution uses an abstract project to isolate OB specific providers from the core app, and also has a [TrueLayer](https://docs.truelayer.com/docs/account-data-overview) project that can use their free sandbox to query account data and transactions.

TrueLayer have to be commended for allowing this with an online signup - the only UK OpenBanking provider that I'm aware of who do this. This is great if you either want to try it out quickly, or write your own home app (as I am doing).

There is also a sample web app that _will_ allow you to report on the banking data you retrieve. That "will" is at the time of writing (July 2022) as it's still very much a work in progress!

